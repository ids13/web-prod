module web-prod

go 1.16

require (
	gitlab.com/boydkelly/julakan-docs.git v0.0.0-20210530094731-bfa49516b47d // indirect
	gitlab.com/hugo-theme/hugo-theme-asciidoc.git v0.0.0-20210126085325-ff401b833624 // indirect
)
