---
categories: [ "Julakan"]
date: 2020-05-08T14:07:30
hyperlink: https://www.mandenkan.com
image: /images/logo-mandenka.jpg
tags: [  "jula", "langue"]
title: Mandenkan
type: notification
---

= http://www.mandenkan.com[Mandenkan]
:author: Boyd Kelly
:email:
:date: 2020-07-06T14:07:30
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula,
:lang: fr 


[width="100%", cols="2", frame="topbot", options"header", stripes="even"]
|====
2+|Ce site appartient à un véritable expert linguistique (karamɔgɔ) et locuteur natif qui offre des cours particuliers à Abidjan, mais aussi des cours à distance par internet ou téléphone.
|Site web: link:http://www.mandenkan.com[Mandenkan]
|Contact: contact@mankenkan.com

|====

