---
title: LEXIQUE DU DIOULA
date: 2020-05-25 
type: notification 
author: COULIBALY Moussa & HARAGUCHI Takehiko 
hyperlink: https://www.coastsystems.net/fr/page/lexiquejula/
aliases:
  - /fr/page/lexiquejula/
  - /en/page/lexiquejula/
  - /dyu/page/lexiquejula/
menu:
  jula:
    title: Lexique du Dioula
---

= LEXIQUE DU DIOULA
:authors: COULIBALY Moussa, HARAGUCHI_Takehiko 
:email: <email@infosomething.jp>
:orgname: Institute of Developing Economies
:sectnums:
:sectnumlevels: 1
:description: Lexique Jula
:page-aliases: lexique:lexiquejula.adoc
:skip-front-matter: true
:lang: fr
include::locale/attributes.adoc[]

****
{authors} +
{orgname} +
****

[preface]
== Préface 
Ce lexique est inspiré du Petit Dictionnaire Dioula que C. Braconnier et M.J. Derive ont publié en 1978.footnote:[C. Braconnier et M. J. Derive. 
_Petit Dictionnaire Dioula._ Institut de Linguistique Appliquée, Abidjan. 1978.]  
Comme le dictionnaire précédent, il voudrait être simplement un outil commode pour les personnes qui souhaitent pouvoir parler dioula en Côte d'Ivoire. 
Il peut être utilisé comme complément à la méthode "kó di? de l'Institut de Linguistique Appliquée d'Abidjan.

NOTE: Par commodité nous avons adopté la même terminologie grammaticale que celle employée dans "kó di ?". Les guillemets signalent une traduction très approximative. 

La forme et le contenu sont en principe les mêmes que pour le "Braconnier et Dérive". Cependant:

. Nous avons enrichi certaines définitions et rectifié d'autres.
. Nous avons ajouté 570 mots(1119 mots au total) qui ont été collectés lors de leçons privées de dioula que Moussa Coulibaly a donnés à Haraguchi Takehiko pendant 2 ans entre 1988 et 1990 à Abidjan.
. Nous avons omis le signe de ton dans les exemples parce qu'il n'est pas d'une grande utilité pratique pour le débutant et que les effort pour l'imprimer correctement sont disproportionnés par rapport à leur utilité.
. Nous avons utilisé les formes élidées résultant de la rencontre de deux voyelles dans les réalisations orales: exemple a b'a ta (a be + a ta).

Nous remercions le professeur Kalilou Tera, chercheur dioulaphone de l'Institut de Linguistique Appliquée qui a vérifié ce lexique et dont les observations et les suggestions nous ont permis de l'améliorer.

[width="30%",cols="1",frame="none",options="none",stripes="none", align="right"]
|===
|octobre 1993
|
|COULIBALY Moussa
|HARAGUCHI Takehiko
|===

.Abreviations
[width="50%",cols="^m,^m",frame="topbot",options="none",stripes="even"]
|====
|pr.|pronom
|pr. réfl.| pronom réfléchi
|s.|singlulier
|pl.|pluriel
|conn. verb.|connectif verbal
|postp.|postposition
|mq. asp.|marque d'aspect
|mq. include.|marque de l'injonctif
|conn synt. compl.|connectif du systagme complétif
|mq. en. adj.|marque d'énoncé adjectival
|par. emph.|particule d'emphase
|mq. inact.|marque d'inactuel
|conn. distrib.|connectif du syntagme distributif
|part. phr.|particule phrastique
|pr. emph.|pronom emphatique
|cop.|copule
|fig.|figurée
|empl. idiom.|emploi idiomatque
|====

== Dioula Français

include::{includedir}vocabulaire.adoc[]

== Français Dioula 

[format="tsv", width="80%",cols="2",frame="topbot",opts="header",stripes="even"]
|====
include::{includedir}francais.tsv[]
|====
