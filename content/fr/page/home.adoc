---
author: 'Boyd Kelly'
date: 2020-05-05T12:11:55
title: "Jamais mieux qu'à l'acceuil !"
type: home
---

= Jamais mieux qu'à l'acceuil !
:author: Boyd Kelly
:email:  
:date: 2020-05-05T12:11:55
:filename: index.adoc
:imagesdir: /images/
:draft: false
:keywords: ["index", "home", "technologie, technology, jula, julakan, dioula, dyula, abidjan, "côte d'ivoire"]
:tags: ["tech", "android", "afrique"]
:topics: ["Technology"]
:lang: fr

image::tai.jpg[]

== Langue et Technologie en Afrique
