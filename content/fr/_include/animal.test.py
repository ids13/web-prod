#!/usr/bin/env python
import os
import pandas as pd

from os import path
from wordcloud import WordCloud

d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()

df = pd.read_csv('animal.tsv', usecols=[0], header=1, sep="\t") 
df.columns =['jula']
print (df)
records = df.to_dict(orient='records')
data = {x['jula'] for x in records}
# Generadte a word cloud image
print (data)
wc = WordCloud(background_color="white", font_path="/usr/share/fonts/google-noto/NotoSans-Medium.ttf", max_words=1000)
wc.generate(data)

wc_svg = wc.to_svg(embed_font=True)
f = open("output.svg","w+")
f.write(wc_svg )
f.close()

# Display the generated image:
# the matplotlib way:
import matplotlib.pyplot as plt

# lower max_font_size
wordcloud = WordCloud(max_font_size=40).generate(text)
plt.figure()
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()

