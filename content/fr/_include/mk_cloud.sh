#!/usr/bin/bash
destdir=images
workdir=chapters/dyu
imagesdir=~/dev/web-prod/static/images
stopwords=./stopwords.txt
fontfile=/usr/share/fonts/google-noto/NotoSans-Thin.ttf
file=$1
awk '{print $1}' $file | wordcloud_cli --fontfile $fontfile --stopwords $stopwords --background "white" --mode RGBA --max_words 100 --imagefile "$imagesdir/${file%.*}.svg"
