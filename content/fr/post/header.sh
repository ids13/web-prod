#!/usr/bin/bash
#Before runing this script ensure the doc has a valid asciidoctor title
#then set the type lang and author defaults

export sectnumlevels=2
export toc="true"
export includedir="include"

[[ -z "$1" ]] && { echo "need a file" ; exit 1 ; }
sed -n '/^=\s/q1' "$1" && { echo "this looks like it doesn't have a title" ; exit 1 ; }
export file=$1
#delete blank lines
sed -i '10,19{/^[[:space:]]*$/d}' "$1" 

export title=`sed -n /^\=\ .*/p "$1"` 
export author=`awk /author:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`
export categories=`awk /categories:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`
export tags=`awk /tags:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`

[[ -z $author ]] && author="Boyd Kelly"
export date=`awk '/date:/{print $2}' "$1" | uniq` 
[[ -z "$date" ]] && date=`date -Im`
export type="post"
export lang="fr"
export draft="true"
[[ -z $categories ]] && categories="[]"
[[ -z $tags ]] && tags="[]"

function yaml {
  sed -i '1 i ---' "$file"
  sed -i "/^---/a title: $title" "$file"
  sed -i "/^title:/a author: $author" "$file"
  sed -i "/^author:/a date: $date" "$file"
  sed -i "/^date:/a type: $type" "$file"
  sed -i "/^type:/a draft: $draft" "$file"
  sed -i "/^draft:/a categories: $categories" "$file"
  sed -i "/^categories:/a tags: $tags" "$file"
  sed -i '/^tags:/a ---' "$file"
  #this is the weirdest hack
  sed -i '9 a #;' "$file" && sed -i 's/^#;//g' "$file"
}

#call yaml function if no yaml header
sed -r -n '/^---(\s|$)/q1' "$1" &&  yaml 

#but if there is already a header it may be missing stuff
#needs to take care of when the key exists but no value...
sed -n '/^title:\s/q1' "$1" &&  sed -i "/^---/a title: $title" "$file"
sed -n '/^author:\s/q1' "$1" &&  sed -i "/^title:/a author: $author" "$file"
[[ ! $(awk '/^date:/{print $2}' "$1") ]] &&  sed -i "/^author:/a date: $date" "$file"
sed -n '/^type:\s/q1' "$1" &&  sed -i "/^date:/a type: $post" "$file"
sed -n '/^categories:\s/q1' "$1" &&  sed -i "/^draft:/a categories: $categories" "$file"
sed -n '/^type:\s/q1' "$1" &&  sed -i "/^date:/a type: $post" "$file"

#start the asciidoctor attributes
sed -i '/^:author:.*/d' "$1" &&  sed -i "/^=\s/a :author: $author" "$1"
#[[ ! $(awk '/^:date:/{print $2}' "$1") ]] && sed -i "/^:author:/a :date: $date" "$1"
sleep 0.25 
sed -i '/^:date:.*/d' "$1" &&  sed -i "/^:author:/a :date: $date" "$1"
sleep 0.25 
sed -i '/^:type:.*/d' "$1" && sed -i "/^:date:/a :type: $type" "$1"
sleep 0.25 
sed -i '/^:toc:.*/d' "$1" &&  sed -i "/^:type:/a :toc: $toc" "$1" 
sleep 0.25 
sed -i '/^:experimental:.*/d' "$1" &&  sed -i '/^:toc:/a :experimental:' "$1" 
sleep 0.25 
sed -i '/^:sectnums:.*/d' "$1" && sed -i '/^:experimental:/a :sectnums:' "$1"
sleep 0.25 
sed -i '/^:sectnumlevels:.*/d' "$1" &&  sed -i "/^:sectnums:/a :sectnumlevels: $sectnumlevels" "$1" 

if [ ! $lang = "en" ]; then
  sed -n '/^:lang:\s/q1' "$1" &&  sed -i "/^:sectnumlevels:/a :lang: $lang" "$1" 
sleep 0.25 
  sed -i '/:includedir:.*/d' "$1" && sed -i "/^:lang:/i :includedir: content/$lang/$includedir" "$1"
sleep 0.25 
  sed -i '/include::locale/d' "$1" &&  sed -i '/^:lang:/a include::locale/attributes.adoc[]' "$1" 
sleep 0.25 
  sed -i "/^include::locale/a #;" "$1" && sed -i 's/^#;//g' "$1"
fi

#sed -i "/^\=\ .*/a #;" "$1"
#sed -i '18 a #;' "$1" && sed -i 's/^#;//g' "$1"
# 

#!/usr/bin/bash
#[[ -z $1 ]] && { echo "need a file" ; exit 1 ; }
#sed -n '/^---\s/q0' $1 || { echo "this looks like it already has a header" ; exit 1 ; }
#this returns 0 when its NOT found.   
#sed -r -n '/^---(\s|$)/q1' "$1" || { echo " This looks like it already has a header" ; exit 1 ;  }
