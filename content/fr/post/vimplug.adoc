---
title:  Gérer les modules vim sans soucis (abandonner le Gestionnaire !) 
date:  2020-05-14T13:21:14
type: post
tags: ["linux", "vim"]
catégories: ["Technologie"]
catégories: ["Technologie"]
image: /images/nvim_94554.png 
---

= Gérer les modules vim sans soucis (abandonner le Gestionnaire !) 
:author: Boyd Kelly
:email:
:date: 2020-05-14T13:21:14
:filename: vimplug.adoc
:description: Abandonner le Gestionnaire ! 
:imagesdir: /images/
:type: post
:lang: fr 
include::./locale/attributes.adoc[fr]

[width="100%",cols="2",frame="topbot",options="noheader,nofooter",stripe="even"]
|====
a|image::vim_94609.png[]
a|image::nvim_94554.png[]
|====

== Gérer les modules Vim et Nvim facilement avec les gestionnaires inégrés !

Les versions récentes de Vim et Nvim sont équipées de gestionnaire de modules, mais ce n'est pas tout le monde qui en profite.
Eh bien voici quelques raisons d'abandonner Pathogen, Vim-Plug, Vundle, et même minpac (qui est quand même basé sur le gestionnaire intégré pour gérer le chemin d'execution)

D'abord quels sont les opérations courantes d'un gestionnaire de modules ?

. Installer un module
. Désinstller un module
. Activer un module
. Désactiver un module
. Lister les modules installés
. Mettre à jour les modules
. Quoi d'autre ?

Certains vont affirmer que vimplug ou bien d'autres gestionnaires simplifient ces taches.
En tant que minimaliste, je ne suis pas d'accord.

J'ai même installé minpack et puis rapidement ne n'en ai pas vu le besoin. Pourquoi donner une instruction à un élément externe pour charger un module que vim peut faire sans *aucune* configuration !
Et la configuration supplémentaire a fini par encombrer mon fichier init.vim avec les instructions de minpac que vim fait lui-même.

Alors si on abandonne son gestionnaires de module préféré, comment on fait ?
Eh bien la plupart des gens utilisent déjà git.
Alors remplacer les quelques 4 ou 5 commandes de votre ancien gestionnaire de module avec une commande git correspondante.  
Et le bienfait supplémentaire c'est que vous n'aurez *rien* de plus dans votre ficher init.
Vous avez déjà installé Vim ou Nvim ?
Laissez le faire le travail ici !
Les commandes git dont vous aurez besoin sont :

****
pluginstall:: git submodule add [url de github]
plugremove:: git submodule deinit {start}/{opt}/[module]
pluglist:: git submodule 
plugdisable:: git submodule mv start/[module] /opt
plugupdate:: git submodule update --remote --recursive
plugsaveall:: git commit -a -m "Installé mon nouveau module" && git push origin master
****

NOTE: Si vous désinstallez un module, vous aurez aussi besoin de lancer une deuxièment commande pour vous débarraser de toutes ses traces :

[source,bash]
----
git rm --cached [chemin du module] 
----

Vous pouvez bien sûr utiliser des alias bash si ces commandes semblent trop longues !

Pour démarrer cette nouvelle façon de faire vous n'avez que vous rendre dans votre répertoie de modules et créer les sous répertoires s'ils n'existe pas déjà.

[source,bash]
----
vim:  cd ~/.vim/pack/plugins
nvim: cd /.local/share/nvim/site/pack/plugins/
mkdir {opt,start}
----

Se nécessaire, vous devez créer un dépot git dans ce répertoire « plugins » et faire un push vers github/gitlab.

[source,bash]
----
git init
git add *
git commit -a -m "mes modules"
git add remote [url du projet de votre compte git]
git push -u orign master
----

Alors ce n'est pas compliqué !

Maintenant si vous êtes sur n'importe quelle autre machine, vous pouvez lancer « git clone myplugins » ;  
Ensuite « git submodule init --recursive..  
Et vous venez d'installer tous vos modules.

L'avantage inattendu de cette façon est que vous n'avez plus de tracas de configuration de votre .vimrc ou init.nvim.

La preuve ?  Voici mon fichier de configuration init.nvim actuel :

[source,vimscript]
----
filetype plugin indent on
syntax enable

set encoding=utf-8
set hidden
set noerrorbells
set smartindent
set smartcase
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set path+=**
set wildmenu
set ts=2
set sw=2
set expandtab
set nowrap

silent! helptags ALL
----

C'est tout !  A+
