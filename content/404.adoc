---
title: Are you lost? Vous êtes perdu ?
date:  2020-05-05T12:11:55
type: 404 
author: Boyd Kelly
---

= Are you lost? Vous êtes perdu ?
:author: Boyd Kelly
:email:  
:date: 2020-05-05T12:11:55
:filename: index.adoc
:imagesdir: /images/
:draft: false
:keywords: ["index", "home", "technologie, technology, jula, julakan, dioula, dyula, abidjan, "côte d'ivoire"]
:tags: ["tech", "android", "afrique"]
:topics: ["Technology"]
:lang: en

image::tai.jpg[]

== The Language, Linux and Tech Grid / Grille de technolgie, Linux, et langues 

* Sorry !  Some things have moved, but everything should be here.   Please check the links in the title bar.

* Pardon ! Les affaires sont déplacés, mais tout est là.   Voir les liens sur la barre de navaigation.
