---
title: Google web fonts for manding languages 
date:  2020-05-08T14:07:30
type: notification 
tags: ["jula", "language"]
categories: ["Julakan" ]
image: /images/polices.png 
hyperlink: https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom 
---

= Google web fonts for manding languages 
:author: Boyd Kelly
:email:
:date: 2020-10-21
:filename: polices.adoc
:description: Web fonts for Bambara, Jula (and other Manding languages)
:imagesdir: /images/
:type: post
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula, polices, fonts, i18n]
:lang: en 
include::locale/attributes.adoc[]

If your are an African web developer go to link:https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Google Fonts] `
to check if your favorite web font contains the appropriate unicode characters to display your language.
