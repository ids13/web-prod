---
title: Font sanity check for Bambara, Jula (and other manding languages)
date:  2020-05-08T14:07:30
type: notification 
tags: [ "jula", "language", "web" ]
categories: ["Julakan" ]
image: /images/polices.png 
hyperlink: https://www.coastsystems.net/dyu/form/ngalonci
---

= Font sanity check for Bambara, Jula (and other manding languages)
:author: Boyd Kelly
:email:
:date: 2020-10-21
:filename: polices.adoc
:imagesdir: /images/
:type: notification 
:keywords: ["Côte d'Ivoire", "Ivory Coast", jula, julakan, dioula, polices, fonts, i18n]
:lang: en 
include::locale/attributes.adoc[]

The internet is international, but not all fonts can display characters from Afrian languages.
link:https://www.coastsystems.net/dyu/ngalonci[Here is a page] where you can check out how a font looks and if it will display the necessary characters used in Manding languages.

