---
author: 'Boyd Kelly'
categories: [ "Technology"]
date: 2020-07-14T08:36:57
draft: false
tags: [ "tech", "vim", "neovim"]
title: 'Nvim plugins'
type: post
---

= Nvim plugins 
:author: Boyd Kelly
:email: 
:date: 2020-05-07T08:36:57
:description:
:filename: nvim-plugins.adoc
:type: post 
:keywords: ["vim", "plugins"]
:tags: ["tech", "vim", "neovim"]
:categories: ["Technology"]
:lang: en 

.Nvim plugins
[width="100%", cols="5", format=csv,frame="topbot",options="header,footer",stripes="even"]
|====
Startup,Plugin,Version,Usage,Notes
include::{includedir}/nvim-plugins.csv[]
|====
