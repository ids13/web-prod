---
categories: [ "Technology"]
date: 2020-11-09
draft: false
lang: en
tags: [ "linux", "fedora"]
title: '"Silverblue Tip - Run commands anywhere"'
type: post
---

= "Silverblue Tip - Run commands anywhere"
:date: 2020-11-09
:lang: en
include::locale/attributes.adoc[]

TIP: Do you find yourself regularly running utilities on your host system as well as in your toolbox? Just put this in your ~/.bashrc:

[source, bash]
----
for bin in fzf nvim code ranger anki meld poedit;
do
type -P $bin >/dev/null 2>&1 || alias $bin="toolbox run $bin" <1>
done
----

<1>  add [ -c toolbox-name ] to run in a non default toolbox

