---
categories: [ "Technology"]
date: 2020-05-14T13:21:14
image: /images/nvim_94554.png
tags: [ "linux", "vim"]
title: 'Simplified Vim plugin management (ditch the manager!)'
type: post
---

= Simplified Vim plugin management (ditch the manager!) 
:author: Boyd Kelly
:email:
:date: 2020-05-14T13:21:14
:filename: vimplug.adoc
:description: Abandonner le Gestionnaire ! 
:imagesdir: /images/
:type: post
:lang: en 
include::./locale/attributes.adoc[fr]

[width="100%", cols="2", frame="topbot", options="noheader,nofooter", stripe="even"]
|====
a|image::vim_94609.png[]
a|image::nvim_94554.png[]
|====

== Simplify your Vim plug-ins with Vim and nvim integrated plug-in managers!

Recent version of Vim and Nvim include a plug-in manager, but it seems that not everyone has yet caught yet. Well here are my reasons to abandon Pathogen, Vimplug, and even minpac (which is based on the new integrated plugin managers and uses the appropriate runtime path)

OK, so what can you do with a plug-in manager?

. Install a plug-in
. Remove a plug-in
. Enable a plug-in
. Disable a plug-in
. List installed plug-ins
. Update installed modules
. Is there really anything else?

Some would argue that vimplug or others simplify these tasks. But as a minimalist, I don't actually think so.

I even installed minpack and then didn't really see the need for it. Why instruct some external piece to load a package when vim does it without any corersion? And the extra configuration just ended up cluttering my init.nvim file with minpack install stuff that git already does.

So if you don't use a plugin manager how do you get along? Well most people use git, and you just replace the 4 or so commands used by your plug-in manager by a corresponding git command. And the added benefit, is you need less or nothing extra in your init file. You have already installed Vim or Nvim. Let Vim or Nvim do the work here! The corresponding commands are:

****
pluginstall:: git submodule add (url from github)
plugremove:: git submodule deinit [start/(opt)/plug-in]
pluglist:: git submodule
plugdisable:: git submodule mv [start/plugin] [opt/plugin]
plugupdate:: git submodule update --remote --recursive
plugsaveall:: git commit -a -m "Install plugin" && git push origin master
****

NOTE: And I do have to add a small caveat here. If you do remove a submodule you will also have to run an additional command to get rid of all traces of it:

[source, bash]
----
git rm --cached [path to module] 
----

Hey you can even use a bash alias if those submodule commands look too long!

To set this up you just need to go to your plug-ins directory and create the directory structure if it doesn't already exist.

[source, bash]
----
mkdir {opt,start}
----

If necessary, you should make this a git repo and push it to github/gitlab.

[source, bash]
----
git init
git add *
git commit -a -m "my plugins"
git add remote [ project url from your git account ]
git push -u origin master
----

So really its not complicated!

Now if you are on any other machine you can just git clone myplugins. Then do a git submodule init --recursive. And you have all your plug ins set up.

The added benefit of this is you now have zero plug-in stuff in your .vimrc or init.nvim.

As proof, Here is the actual contents of my init.nvim:

[source, vimscript]
----
filetype plugin indent on
syntax enable

set encoding=utf-8
set hidden
set noerrorbells
set smartindent
set smartcase
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set path+=**
set wildmenu
set ts=2
set sw=2
set expandtab
set nowrap

silent! helptags ALL
----

That's all folks!
