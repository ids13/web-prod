#!/usr/bin/bash
rm site-*.scss
for x in `ls *.css`; do 
  out=site-${x%.*}.scss
  sed -n /@import/p $x > ${out}
  sed -n /^html/p ${x} >> ${out}
  sed -n /^kbd/p ${x} >> ${out}
  sed -n /^h1/p ${x} >> ${out}
  sed -n 's/^h1.*font-family.*$/html, body, nav, &/p' ${x} >> ${out}
  sed -n /^img/p ${x} >> ${out}
  sed -n '/^a img/p' ${x} >> ${out}
  sed -n '/^.sidebarblock/p' ${x} >> ${out}

# This shouldn't really change between themes, but it would be nice to sed it from each file later
cat <<'EOD' >> ${out}
@media only screen and (min-width: 768px) { h1, h2, h3, #toctitle, .sidebarblock > .content > .title, h4, h5, h6 { line-height: 1.25; }
  h1 { font-size: 2.75em; }
  h2 { font-size: 2.3125em; }
  h3, #toctitle, .sidebarblock > .content > .title { font-size: 1.6875em; }
  h4 { font-size: 1.4375em; } }
  /* need to fix this as body and html get weird values normally with h1-h6 etc above */
  /* fix this properly to get font-family for html and body with sed later */
  html, body {
  font-size: 100%;
  line-height: 1.2rem;
  margin: 0;
}
EOD

cat <<DOE >> ${out}
.doc {
  @import "./${x%.*}";
}
DOE
#make table-cells block on mobile; This is *after import .doc as it is for the documents not the site
cat <<EOD1 >> ${out}
@media only screen and (max-width: 1023px) {
table thead tr th,table tfoot tr th,table tbody tr td,table tr td,table tfoot tr td{display:block;}
}
EOD1

done 

