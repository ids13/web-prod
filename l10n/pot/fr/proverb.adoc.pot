# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-04-26 08:46+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: YAML Front Matter: draft
#: content/fr/post/fonts.adoc:1 content/fr/post/nvim-plugins.adoc:1
#: content/fr/post/proverb.adoc:1 content/fr/post/ngalonci.adoc:1
#: content/fr/post/fontlist.adoc:1 content/fr/post/sb-bashrc.adoc:1
#: content/fr/post/2020-12-06-w100.adoc:1
#: content/fr/post/2021-01-02-adoc-vim.adoc:1
#: content/fr/post/2021-04-23-animal.adoc:1
#, no-wrap
msgid "false"
msgstr ""

#. type: YAML Front Matter: author
#: content/fr/post/proverb.adoc:1
#, no-wrap
msgid "Molière"
msgstr ""

#. type: Attribute :catégories:
#: content/fr/post/proverb.adoc:1 content/fr/post/ysa.adoc:1
#: content/fr/post/justdoit.adoc:1 content/fr/post/justdoit.adoc:17
#, no-wrap
msgid "[\"Anglais en français\"]"
msgstr ""

#. type: YAML Front Matter: date
#: content/fr/post/proverb.adoc:1
#, no-wrap
msgid "2020-06-06T11:11:52Z"
msgstr ""

#. type: Attribute :lang:
#: content/fr/post/proverb.adoc:1 content/fr/post/clavier-android.adoc:13
#: content/fr/post/ngalonci.adoc:1 content/fr/post/ngalonci.adoc:18
#: content/fr/post/ponctuation-francaise.adoc:23 content/fr/post/anki.adoc:14
#: content/fr/post/website.adoc:1 content/fr/post/website.adoc:14
#: content/fr/post/sb-bashrc.adoc:1 content/fr/post/sb-bashrc.adoc:13
#: content/fr/page/2018-07-30-alpha-jula.adoc:20
#, no-wrap
msgid "fr"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/post/proverb.adoc:1
#, no-wrap
msgid "[\"proverbe\", \"français\"]"
msgstr ""

#. type: YAML Front Matter: title
#: content/fr/post/proverb.adoc:1
#, no-wrap
msgid "Qui se sent morveux..."
msgstr ""

#. type: Title =
#: content/fr/post/proverb.adoc:12
#, no-wrap
msgid "Qui se sent morveux se mouche "
msgstr ""

#. type: delimited block _
#: content/fr/post/proverb.adoc:17
msgid "Qui se sent morveux se mouche."
msgstr ""
