# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2020-12-07 12:08+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: content/fr/post/mogo.adoc:1 content/fr/post/fonts.adoc:1
#: content/fr/post/nvim-plugins.adoc:1 content/fr/post/vimplug.adoc:1
#: content/fr/post/proverb.adoc:1 content/fr/post/a-sen-b-a-la.adoc:1
#: content/fr/post/clavier-android.adoc:1 content/fr/post/ngalonci.adoc:1
#: content/fr/post/alpha-jula.adoc:1
#: content/fr/post/ponctuation-francaise.adoc:1 content/fr/post/ysa.adoc:1
#: content/fr/post/anki.adoc:1 content/fr/post/justdoit.adoc:1
#: content/fr/post/website.adoc:1 content/fr/post/datally.adoc:1
#: content/fr/post/clavier-ivoirien.adoc:1 content/fr/post/fontlist.adoc:1
#: content/fr/post/sb-bashrc.adoc:1 content/fr/post/money.adoc:1
#: content/fr/post/2020-12-06-w100.adoc:1
#, no-wrap
msgid "---\n"
msgstr ""

#. type: Attribute :lang:
#: content/fr/post/mogo.adoc:15 content/fr/post/fonts.adoc:20
#: content/fr/post/nvim-plugins.adoc:21 content/fr/post/vimplug.adoc:18
#: content/fr/post/a-sen-b-a-la.adoc:14 content/fr/post/alpha-jula.adoc:15
#: content/fr/post/ysa.adoc:14 content/fr/post/justdoit.adoc:13
#: content/fr/post/datally.adoc:16 content/fr/post/clavier-ivoirien.adoc:17
#: content/fr/post/fontlist.adoc:23 content/fr/post/money.adoc:19
#, no-wrap
msgid "fr "
msgstr ""

#. type: Attribute :categories:
#: content/fr/post/mogo.adoc:17 content/fr/post/money.adoc:18
#: content/fr/post/2020-12-06-w100.adoc:21
#, no-wrap
msgid "[\"Julakan\"]"
msgstr ""

#. type: Plain text
#: content/fr/post/money.adoc:7
#, no-wrap
msgid ""
"title: L'argent et chiffres en Jula\n"
"date: 2020-11-24\n"
"author: Boyd Kelly\n"
"tags: [ \"argent\", \"chiffres\" ]\n"
"categories: [ \"Julakan\" ]\n"
"---\n"
msgstr ""

#. type: Block title
#: content/fr/post/money.adoc:9 content/fr/post/money.adoc:24
#, no-wrap
msgid "L'argent et chiffres en Jula"
msgstr ""

#. type: Attribute :tags:
#: content/fr/post/money.adoc:17
#, no-wrap
msgid "[\"argent\", \"chiffres\"]"
msgstr ""

#. type: Table
#: content/fr/post/money.adoc:28
#, no-wrap
msgid "include::{includedir}money.csv[]\n"
msgstr ""
