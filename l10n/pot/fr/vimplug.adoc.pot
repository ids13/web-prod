# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-04-26 08:46+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: YAML Front Matter: type
#: content/fr/post/mogo.adoc:1 content/fr/post/fonts.adoc:1
#: content/fr/post/nvim-plugins.adoc:1 content/fr/post/vimplug.adoc:1
#: content/fr/post/a-sen-b-a-la.adoc:1 content/fr/post/clavier-android.adoc:1
#: content/fr/post/ngalonci.adoc:1 content/fr/post/ponctuation-francaise.adoc:1
#: content/fr/post/ysa.adoc:1 content/fr/post/anki.adoc:1
#: content/fr/post/justdoit.adoc:1 content/fr/post/website.adoc:1
#: content/fr/post/datally.adoc:1 content/fr/post/clavier-ivoirien.adoc:1
#: content/fr/post/fontlist.adoc:1 content/fr/post/sb-bashrc.adoc:1
#: content/fr/post/2020-12-06-w100.adoc:1
#: content/fr/post/2021-01-02-adoc-vim.adoc:1
#: content/fr/post/2021-04-23-animal.adoc:1
#, no-wrap
msgid "post"
msgstr ""

#. type: Attribute :lang:
#: content/fr/post/mogo.adoc:15 content/fr/post/fonts.adoc:21
#: content/fr/post/nvim-plugins.adoc:21 content/fr/post/vimplug.adoc:19
#: content/fr/post/a-sen-b-a-la.adoc:14 content/fr/post/ysa.adoc:14
#: content/fr/post/justdoit.adoc:13 content/fr/post/datally.adoc:16
#: content/fr/post/clavier-ivoirien.adoc:17 content/fr/post/fontlist.adoc:19
#: content/fr/post/2020-12-06-w100.adoc:23
#: content/fr/post/2021-01-02-adoc-vim.adoc:21
#: content/fr/post/2021-04-23-animal.adoc:23
#, no-wrap
msgid "fr "
msgstr ""

#. type: Attribute :catégories:
#: content/fr/post/nvim-plugins.adoc:1 content/fr/post/nvim-plugins.adoc:20
#: content/fr/post/vimplug.adoc:1 content/fr/post/ponctuation-francaise.adoc:1
#: content/fr/post/ponctuation-francaise.adoc:22 content/fr/post/website.adoc:1
#: content/fr/post/datally.adoc:1 content/fr/post/datally.adoc:15
#: content/fr/post/clavier-ivoirien.adoc:1
#: content/fr/post/clavier-ivoirien.adoc:20 content/fr/post/sb-bashrc.adoc:1
#: content/fr/post/2021-01-02-adoc-vim.adoc:1
#: content/fr/post/2021-01-02-adoc-vim.adoc:20
#, no-wrap
msgid "[\"Technologie\"]"
msgstr ""

#. type: YAML Front Matter: date
#: content/fr/post/vimplug.adoc:1
#, no-wrap
msgid "2020-05-14T13:21:14"
msgstr ""

#. type: YAML Front Matter: image
#: content/fr/post/vimplug.adoc:1
#, no-wrap
msgid "/images/nvim_94554.png"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/post/vimplug.adoc:1
#, no-wrap
msgid "[\"linux\", \"vim\"]"
msgstr ""

#. type: YAML Front Matter: title
#: content/fr/post/vimplug.adoc:1
#, no-wrap
msgid "Gérer les modules vim sans soucis (abandonner le Gestionnaire !)"
msgstr ""

#. type: Title =
#: content/fr/post/vimplug.adoc:11
#, no-wrap
msgid "Gérer les modules vim sans soucis (abandonner le Gestionnaire !) "
msgstr ""

#. type: Table
#: content/fr/post/vimplug.adoc:26
#, no-wrap
msgid ""
"a|image::vim_94609.png[]\n"
"a|image::nvim_94554.png[]\n"
msgstr ""

#. type: Title ==
#: content/fr/post/vimplug.adoc:28
#, no-wrap
msgid "Gérer les modules Vim et Nvim facilement avec les gestionnaires inégrés !"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:32
msgid ""
"Les versions récentes de Vim et Nvim sont équipées de gestionnaire de "
"modules, mais ce n'est pas tout le monde qui en profite.  Eh bien voici "
"quelques raisons d'abandonner Pathogen, Vim-Plug, Vundle, et même minpac "
"(qui est quand même basé sur le gestionnaire intégré pour gérer le chemin "
"d'execution)"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:34
msgid ""
"D'abord quels sont les opérations courantes d'un gestionnaire de modules ?"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:36
msgid "Installer un module"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:37
msgid "Désinstller un module"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:38
msgid "Activer un module"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:39
msgid "Désactiver un module"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:40
msgid "Lister les modules installés"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:41
msgid "Mettre à jour les modules"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:42
msgid "Quoi d'autre ?"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:45
msgid ""
"Certains vont affirmer que vimplug ou bien d'autres gestionnaires "
"simplifient ces taches.  En tant que minimaliste, je ne suis pas d'accord."
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:48
msgid ""
"J'ai même installé minpack et puis rapidement ne n'en ai pas vu le besoin. "
"Pourquoi donner une instruction à un élément externe pour charger un module "
"que vim peut faire sans *aucune* configuration ! Et la configuration "
"supplémentaire a fini par encombrer mon fichier init.vim avec les "
"instructions de minpac que vim fait lui-même."
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:56
msgid ""
"Alors si on abandonne son gestionnaires de module préféré, comment on fait ? "
"Eh bien la plupart des gens utilisent déjà git.  Alors remplacer les "
"quelques 4 ou 5 commandes de votre ancien gestionnaire de module avec une "
"commande git correspondante.  Et le bienfait supplémentaire c'est que vous "
"n'aurez *rien* de plus dans votre ficher init.  Vous avez déjà installé Vim "
"ou Nvim ? Laissez le faire le travail ici ! Les commandes git dont vous "
"aurez besoin sont :"
msgstr ""

#. type: Labeled list
#: content/fr/post/vimplug.adoc:58
#, no-wrap
msgid "pluginstall"
msgstr ""

#. type: delimited block *
#: content/fr/post/vimplug.adoc:59
msgid "git submodule add [url de github]"
msgstr ""

#. type: Labeled list
#: content/fr/post/vimplug.adoc:59
#, no-wrap
msgid "plugremove"
msgstr ""

#. type: delimited block *
#: content/fr/post/vimplug.adoc:60
msgid "git submodule deinit {start}/{opt}/[module]"
msgstr ""

#. type: Labeled list
#: content/fr/post/vimplug.adoc:60
#, no-wrap
msgid "pluglist"
msgstr ""

#. type: delimited block *
#: content/fr/post/vimplug.adoc:61
msgid "git submodule"
msgstr ""

#. type: Labeled list
#: content/fr/post/vimplug.adoc:61
#, no-wrap
msgid "plugdisable"
msgstr ""

#. type: delimited block *
#: content/fr/post/vimplug.adoc:62
msgid "git submodule mv start/[module] /opt"
msgstr ""

#. type: Labeled list
#: content/fr/post/vimplug.adoc:62
#, no-wrap
msgid "plugupdate"
msgstr ""

#. type: delimited block *
#: content/fr/post/vimplug.adoc:63
msgid "git submodule update --remote --recursive"
msgstr ""

#. type: Labeled list
#: content/fr/post/vimplug.adoc:63
#, no-wrap
msgid "plugsaveall"
msgstr ""

#. type: delimited block *
#: content/fr/post/vimplug.adoc:64
msgid ""
"git commit -a -m \"Installé mon nouveau module\" && git push origin master"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:67
msgid ""
"Si vous désinstallez un module, vous aurez aussi besoin de lancer une "
"deuxièment commande pour vous débarraser de toutes ses traces :"
msgstr ""

#. type: delimited block -
#: content/fr/post/vimplug.adoc:71
#, no-wrap
msgid "git rm --cached [chemin du module] \n"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:74
msgid ""
"Vous pouvez bien sûr utiliser des alias bash si ces commandes semblent trop "
"longues !"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:76
msgid ""
"Pour démarrer cette nouvelle façon de faire vous n'avez que vous rendre dans "
"votre répertoie de modules et créer les sous répertoires s'ils n'existe pas "
"déjà."
msgstr ""

#. type: delimited block -
#: content/fr/post/vimplug.adoc:82
#, no-wrap
msgid ""
"vim:  cd ~/.vim/pack/plugins\n"
"nvim: cd /.local/share/nvim/site/pack/plugins/\n"
"mkdir {opt,start}\n"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:85
msgid ""
"Se nécessaire, vous devez créer un dépot git dans ce répertoire « plugins » "
"et faire un push vers github/gitlab."
msgstr ""

#. type: delimited block -
#: content/fr/post/vimplug.adoc:93
#, no-wrap
msgid ""
"git init\n"
"git add *\n"
"git commit -a -m \"mes modules\"\n"
"git add remote [url du projet de votre compte git]\n"
"git push -u orign master\n"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:96
msgid "Alors ce n'est pas compliqué !"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:100
msgid ""
"Maintenant si vous êtes sur n'importe quelle autre machine, vous pouvez "
"lancer « git clone myplugins » ;  Ensuite « git submodule init --"
"recursive..  Et vous venez d'installer tous vos modules."
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:102
msgid ""
"L'avantage inattendu de cette façon est que vous n'avez plus de tracas de "
"configuration de votre .vimrc ou init.nvim."
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:104
msgid "La preuve ? Voici mon fichier de configuration init.nvim actuel :"
msgstr ""

#. type: delimited block -
#: content/fr/post/vimplug.adoc:109
#, no-wrap
msgid ""
"filetype plugin indent on\n"
"syntax enable\n"
msgstr ""

#. type: delimited block -
#: content/fr/post/vimplug.adoc:125
#, no-wrap
msgid ""
"set encoding=utf-8\n"
"set hidden\n"
"set noerrorbells\n"
"set smartindent\n"
"set smartcase\n"
"set nobackup\n"
"set undodir=~/.config/nvim/undodir\n"
"set undofile\n"
"set incsearch\n"
"set path+=**\n"
"set wildmenu\n"
"set ts=2\n"
"set sw=2\n"
"set expandtab\n"
"set nowrap\n"
msgstr ""

#. type: delimited block -
#: content/fr/post/vimplug.adoc:127
#, no-wrap
msgid "silent! helptags ALL\n"
msgstr ""

#. type: Plain text
#: content/fr/post/vimplug.adoc:129
msgid "C'est tout ! A+"
msgstr ""
