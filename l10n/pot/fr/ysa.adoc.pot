# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-04-26 08:46+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: YAML Front Matter: type
#: content/fr/post/mogo.adoc:1 content/fr/post/fonts.adoc:1
#: content/fr/post/nvim-plugins.adoc:1 content/fr/post/vimplug.adoc:1
#: content/fr/post/a-sen-b-a-la.adoc:1 content/fr/post/clavier-android.adoc:1
#: content/fr/post/ngalonci.adoc:1 content/fr/post/ponctuation-francaise.adoc:1
#: content/fr/post/ysa.adoc:1 content/fr/post/anki.adoc:1
#: content/fr/post/justdoit.adoc:1 content/fr/post/website.adoc:1
#: content/fr/post/datally.adoc:1 content/fr/post/clavier-ivoirien.adoc:1
#: content/fr/post/fontlist.adoc:1 content/fr/post/sb-bashrc.adoc:1
#: content/fr/post/2020-12-06-w100.adoc:1
#: content/fr/post/2021-01-02-adoc-vim.adoc:1
#: content/fr/post/2021-04-23-animal.adoc:1
#, no-wrap
msgid "post"
msgstr ""

#. type: Attribute :lang:
#: content/fr/post/mogo.adoc:15 content/fr/post/fonts.adoc:21
#: content/fr/post/nvim-plugins.adoc:21 content/fr/post/vimplug.adoc:19
#: content/fr/post/a-sen-b-a-la.adoc:14 content/fr/post/ysa.adoc:14
#: content/fr/post/justdoit.adoc:13 content/fr/post/datally.adoc:16
#: content/fr/post/clavier-ivoirien.adoc:17 content/fr/post/fontlist.adoc:19
#: content/fr/post/2020-12-06-w100.adoc:23
#: content/fr/post/2021-01-02-adoc-vim.adoc:21
#: content/fr/post/2021-04-23-animal.adoc:23
#, no-wrap
msgid "fr "
msgstr ""

#. type: Attribute :catégories:
#: content/fr/post/proverb.adoc:1 content/fr/post/ysa.adoc:1
#: content/fr/post/justdoit.adoc:1 content/fr/post/justdoit.adoc:17
#, no-wrap
msgid "[\"Anglais en français\"]"
msgstr ""

#. type: YAML Front Matter: date
#: content/fr/post/ysa.adoc:1
#, no-wrap
msgid "2018-05-21 22:46:26 +0000"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/post/ysa.adoc:1
#, no-wrap
msgid "[\"langue\", \"français\", \"anglais\"]"
msgstr ""

#. type: Title =
#: content/fr/post/ysa.adoc:1 content/fr/post/ysa.adoc:9
#, no-wrap
msgid "Yet Still Again ?"
msgstr ""

#. type: Title ==
#: content/fr/post/ysa.adoc:17
#, no-wrap
msgid "Quelle est la différence entre ces trois termes ?"
msgstr ""

#. type: Plain text
#: content/fr/post/ysa.adoc:22
msgid ""
"Souvent le français est plus précis que l'anglais... Surtout en matière de "
"verbes.  Par contre l'anglais peut aussi être plus précis que le français."
msgstr ""

#. type: Title ==
#: content/fr/post/ysa.adoc:23
#, no-wrap
msgid "Voici un exemple"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:25
#, no-wrap
msgid "Yet"
msgstr ""

#. type: Plain text
#: content/fr/post/ysa.adoc:26 content/fr/post/ysa.adoc:27
#: content/fr/post/ysa.adoc:28 content/fr/post/ysa.adoc:33
msgid "Encore"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:26
#, no-wrap
msgid "Still"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:27
#, no-wrap
msgid "Again"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:28
#, no-wrap
msgid "Even"
msgstr ""

#. type: Plain text
#: content/fr/post/ysa.adoc:29
msgid "Encore (souvent traduit par 'même', mais parfois 'encore')"
msgstr ""

#. type: Plain text
#: content/fr/post/ysa.adoc:31
msgid ""
"et encore.footnote:[Dans link:https://www.coastsystems.net/en/post/ysa[la "
"version anglaise] de ce document j'ai traduit 'encore' par 'aussi'.] :"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:32
#, no-wrap
msgid "Yet again"
msgstr ""

#. type: Plain text
#: content/fr/post/ysa.adoc:36
msgid ""
"Oui, parfois on trouve en anglais les deux mots ensemble dans une phrase : "
"'*yet again*...' Qu'est-ce que cela signifie ?"
msgstr ""

#. type: Plain text
#: content/fr/post/ysa.adoc:39
msgid ""
"Nous avons non seulement quatre mots en anglais, mais quatre significations "
"différentes avec la même traduction en français.  Connaissez-vous la nuance?"
msgstr ""

#. type: Block title
#: content/fr/post/ysa.adoc:40
#, no-wrap
msgid "Voici quelques phrases pour vous aider:"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:42
#, no-wrap
msgid "She hasn't come yet"
msgstr ""

#. type: delimited block *
#: content/fr/post/ysa.adoc:43 content/fr/post/ysa.adoc:44
msgid "Elle n'est pas encore venue"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:43
#, no-wrap
msgid "She is yet to come"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:44
#, no-wrap
msgid "She is still sleeping"
msgstr ""

#. type: delimited block *
#: content/fr/post/ysa.adoc:45
msgid "Elle dort encore.  (Elle continue à dormir)"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:45
#, no-wrap
msgid "She is eating again"
msgstr ""

#. type: delimited block *
#: content/fr/post/ysa.adoc:46
msgid "Elle mange encore.  (Elle mange de nouveau)"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:46
#, no-wrap
msgid "She is going yet again"
msgstr ""

#. type: delimited block *
#: content/fr/post/ysa.adoc:47
msgid "Elle va encore.  (Elle continue d'y aller une fois de plus)"
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:47
#, no-wrap
msgid "It's even more difficult than I thought"
msgstr ""

#. type: delimited block *
#: content/fr/post/ysa.adoc:48
msgid "C'est encore plus difficile que je ne le pensais."
msgstr ""

#. type: Labeled list
#: content/fr/post/ysa.adoc:48
#, no-wrap
msgid "There's even more to come"
msgstr ""

#. type: delimited block *
#: content/fr/post/ysa.adoc:49
msgid "Il y en a encore davantage à venir."
msgstr ""

#. type: Table
#: content/fr/post/ysa.adoc:58
#, no-wrap
msgid ""
"|Yet|Une action qui ne s'est pas produit jusqu'au présent. (encore)\n"
"|Still|Une action qui continue à se produire, qui n'a jamais arrêté.\n"
"|Again|Une action discontinue qui reprend.\n"
"|Even|Nuancé avec 'still'|Une chose qui s'est arrêtée/épuisée, mais il y en aura...|(encore !)\n"
"|Yet again|Une action discontinue qui reprend au moins une fois mais probablement plus.\n"
msgstr ""
