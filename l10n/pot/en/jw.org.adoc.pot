# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-04-22 13:37+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: YAML Front Matter: headless
#: content/en/post/smoketest.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/page/_index.adoc:1
#, no-wrap
msgid "false"
msgstr ""

#. type: YAML Front Matter: categories
#: content/en/resources/01-mediaplus.adoc:1
#: content/en/resources/ankataa.com.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#, no-wrap
msgid "[\"Julakan\" ]"
msgstr ""

#. type: YAML Front Matter: type
#: content/en/resources/01-mediaplus.adoc:1
#: content/en/resources/ankataa.com.adoc:1
#: content/en/resources/bebiphilip.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#: content/en/page/2020-11-24-money.adoc:1
#, no-wrap
msgid "notification"
msgstr ""

#. type: YAML Front Matter: date
#: content/en/resources/ankataa.com.adoc:1 content/en/resources/bible.adoc:1
#: content/en/resources/mandenkan.com.adoc:1 content/en/resources/rfi.adoc:1
#: content/en/resources/jw.org.adoc:1 content/en/resources/fonts_google.adoc:1
#: content/en/resources/fonts_ngalonci.adoc:1
#, no-wrap
msgid "2020-05-08T14:07:30"
msgstr ""

#. type: Attribute :tags:
#: content/en/resources/ankataa.com.adoc:1 content/en/resources/jw.org.adoc:1
#: content/en/resources/jw.org.adoc:20
#, no-wrap
msgid "[ \"jula\", \"language\" ]"
msgstr ""

#. type: Attribute :lang:
#: content/en/resources/ankataa.com.adoc:21
#: content/en/resources/bebiphilip.adoc:22 content/en/resources/bible.adoc:21
#: content/en/resources/mandenkan.com.adoc:20 content/en/resources/rfi.adoc:20
#: content/en/resources/jw.org.adoc:22
#: content/en/resources/fonts_google.adoc:20
#: content/en/resources/fonts_ngalonci.adoc:19
#: content/en/page/2020-11-24-money.adoc:25
#, no-wrap
msgid "en "
msgstr ""

#. type: YAML Front Matter: hyperlink
#: content/en/resources/jw.org.adoc:1
#, no-wrap
msgid "https://www.jw.org/dyu"
msgstr ""

#. type: YAML Front Matter: image
#: content/en/resources/jw.org.adoc:1
#, no-wrap
msgid "/images/jw.org.jpeg"
msgstr ""

#. type: YAML Front Matter: title
#: content/en/resources/jw.org.adoc:1
#, no-wrap
msgid "JW.org in Bambara, French and Jula"
msgstr ""

#. type: Title =
#: content/en/resources/jw.org.adoc:12
#, no-wrap
msgid "title: JW.org in Bambara, French and Jula "
msgstr ""

#. type: Table
#: content/en/resources/jw.org.adoc:31
#, no-wrap
msgid ""
"3+|These sites contain articles, videos and audio tracks in Burkina Faso Jula, that you can compare to the same in Bambara or English.\n"
"|link:https://www.jw.org/bm[jw.org in Bambara] \n"
"|link:https://www.jw.org/fr[jw.org in French]\n"
"|link:https://www.jw.org/dyu[jw.org in jula] \n"
msgstr ""
