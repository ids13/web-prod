��          |      �             !     5  �  I     �  ,        3     J     W     [     s     �  �   �     �     �    �     �  ,   �               #     '     ?     L                     	                 
                     /images/ankataa.png 2020-05-08T14:07:30 3+|An expert linguist providing online courses, in depth research as well an awesome Youtube channel for learning basic Jula and Bambara. 
Great for Jula learniers in Ivory Coast. (Just substitute 'ka' for ye, and 'le' or 'lo' for 'don'...  and a few others but after all this *is* mandenkan!)
|Web site:link:https://www.ankataa.com[Ankataa]
|
|Youtube channel:link:https://www.youtube.com/channel/UCEQgnXDXNHaAjKA8GJZ3zHw[Na baro kɛ]
 Ankataa Le site web https://www.ankataa.com[Ankataa] [ "jula", "language" ] ["Julakan" ] en  https://www.ankataa.com notification tags: [ "jula", "language" ] Project-Id-Version: 
PO-Revision-Date: 2021-04-22 13:44+0000
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: en
X-Generator: Poedit 2.4.2
 /images/ankataa.png 2020-05-08T14:07:30 3+|Vous trouverez ici, des cours en ligne, des recherches approfondies, ainsi qu'une chaine Youtube pour vous aider à apprendre le Jula et let Bambara. 
Formidable pour les apprenants de Jula de Côte d'Ivoire.  (Seulement remplacer le 'ye' par 'ka', le 'don' par 'le' ou 'lo' et vous êtes prêts... eh bien peut-être queques autres éléments, mais d'après tout c'est le Mandenkan!
|Site web: :link:https://www.ankataa.com[Ankataa]
|Chaine Youtube: link:https://www.youtube.com/channel/UCEQgnXDXNHaAjKA8GJZ3zHw[Na baro kɛ]
 Ankataa Le site web https://www.ankataa.com[Ankataa] [ "jula", "langue"] ["Julakan"] fr  https://www.ankataa.com notification [ "jula", "langue"] 