��          |      �             !  	  &  �   0  n   !    �  3   �  �   �  t   �  	   *     4  _   7  �   �     �  	  �  �   �  n   �	  4  �	  b   2  "  �  �   �     F     T  j   W                              	               
              ---
 <div class="LI-profile-badge"  data-version="v1" data-size="medium" data-./locale="en_US" data-type="horizontal" data-theme="light" data-vanity="boydkelly"><a class="LI-simple-link" href='https://ca.linkedin.com/in/boydkelly?trk=profile-badge'>Boyd Kelly</a></div>
 <figure class="image">
<div src="https://docs.google.com/forms/d/e/1FAIpQLSdw6yhla0-mmVrAWeLcHM2lBKHvKZre4uiiiGCjvaG30x22Qg/viewform?embedded=true" width="700" height="1000" frameborder="0" marginheight="0" marginwidth="0"></div>
</figure>
 <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
 I have hosted websites, managed email and dns servers, coded, and taught a MS Exchange course at the University of British Columbia.  I have a lot of experience in virtual technologies on Linux, Amazon and GCP. Working remotely with full confidence is a given. If you would like to hire me, let’s get in touch. In short, I have done extensive volunteer service including IT work, managed the help desk for the Canadian Passport Office, and worked in a large corporate IT department.  That’s experience in volunteer, public, and private sectors. This static website is hosted on Gitlab, and created by me using Asciidoctor.  The DNS is hosted on Amazon Route 53. Who am I? en title:  Who am I?
date:  2018-05-21T21:18:02Z
type: page
translationkey: about
slug: about
---
 Project-Id-Version: 
PO-Revision-Date: 2021-01-07 00:18+0000
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: en
X-Generator: Poedit 2.4.2
 ---
 <div class="LI-profile-badge"  data-version="v1" data-size="medium" data-./locale="en_US" data-type="horizontal" data-theme="light" data-vanity="boydkelly"><a class="LI-simple-link" href='https://ca.linkedin.com/in/boydkelly?trk=profile-badge'>Boyd Kelly</a></div>
 <figure class="image">
<div src="https://docs.google.com/forms/d/e/1FAIpQLSdw6yhla0-mmVrAWeLcHM2lBKHvKZre4uiiiGCjvaG30x22Qg/viewform?embedded=true" width="700" height="1000" frameborder="0" marginheight="0" marginwidth="0"></div>
</figure>
 <script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script>
 Au cours de route j'ai hébergé des sites web, géré les servers mail et DNS et enseigné les cours de Microsoft Exchange à l'Université de la Columbie Britanique.  Avec beaucoup d'experience dans les technologies virtuelles sur Linux, Amazon et GCE, je peux travailler à distance avec pleine confiance. Si vous cherchez un technicien qualifié et digne de confiance, envoyez-moi un message ci-dessous. En sommaire, j'ai fait du bénévolat extensif en afrique, tout en travaillant avec la technologie.  J'ai supervisé departement de support technique pour le Bureau des Passports du Canada à l'ouest et ensuite travaillé dans le departement informatique d'une grande société de logiciel. Ce site web static est hébérgé sur Gitlab, créée par moi avec Hugo , Asciidoctor et neovim .  Le DNS est hébergé chez Amazon Route 53. Qui suis-je ? fr title:  Qui suis-je ?
date:  2018-05-21 21:18:02 +0000
type: page
translationkey: about
slug: apropos
---
 