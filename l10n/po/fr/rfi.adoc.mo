��    
      l      �       �      �          �  $   �     �     �     �     �       �          �       �  $   �     	          )     -     C                  	      
                     /images/rfi_mandenkan.jpg 2+|Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.
Based in Bambako, but broadcast to Mandenkan listers across West Africa.  Updated daily.
10 min news cast is followed by 20 min of interviews and discussion from Monday to Friday and 8:00 AM and 12:00 PM.
|Web site: link:http://rfi.fr/ma[rfi]
|
 2020-05-08T14:07:30 Radio France International Mandenkan [ "jula", "language"] ["Julakan" ] en  https://www.rfi.fr/ma notification Project-Id-Version: 
PO-Revision-Date: 2021-04-22 12:29+0000
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: en
X-Generator: Poedit 2.4.2
 /images/rfi_mandenkan.jpg 2+|Ecoutez l’actualité internationale, africaine, française et régionale (Afrique de l’Ouest) en mandenkan, ainsi que le sport, une revue de presse régionale.  Basé à Bambako, mais diffusé aux locuteurs mandenkan en Afrique de l'ouest. Mise-à-jour quotidiennement.  Les infos en 10 minutes, suivi par une émission d'actualité de 20 minutes du lundi au vendredi à 8h00 et 12h00.
|Site web: link:http://rfi.fr/ma[rfi]

 2020-05-08T14:07:30 Radio France International Mandenkan [ "jula", "langue"] ["Julakan"] fr  https://www.rfi.fr/ma notification 