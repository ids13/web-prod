��          �      L      �     �  
   �  .  �  �     �  �  r  �    	  '   
  W   A
  �   �
  (   u     �     �     �  .   �     �     �  m     �   r     i  
   }  *  �  �   �  X  �  �  �  L  �  '   �  A   �  �   7  '   �          ,     2  .   6     e     q  m   v                        
   	                                                                         /images/polices.png 2020-10-21 Ce site web utilse link:https://fonts.google.com/specimen/Noto+Sans?preview.text=%C9%94%20%200254%20%C6%86%20%200186%20%C9%9B%20%20025B%20%C6%90%20%200190%20%20%20%C9%B2%20%200272%20%C6%9D%20%20019D%20%20%C5%8B%20%20014B%20%C5%8A%20%20014A&preview.text_type=custom&query=noto[Noto Sans] comme défault. Cherchez d"autres polices web sur... link:https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Google Fonts] En fait, le navigateur substitue généralement une police d'éctiture système pour rendre des glyphes manquants.  Souvent cette substitution de glyphes d'une autre police système peut donner un résult acceptable ou même très bien.  Par contre pour ne jamais avoir de surprises dans la présentation de votre application ou page web, choisessz des polices adaptées à la langue ! La police web la plus populaire chez Google qui gère tous les glyphes IPA c'est Noto Sans ou bien Noto Serif.  Bien qu'on parle de langues mandingues ici, si votre text est en Sénari(Senoufo) ou bien Wawle(Baoulé) ou encore ˈBhɛtɩgbʋʋ (Bete), le choix de police devient encore plus important. L'internet est international, mais ce n'est pas toutes les polices.footnote:[En typographie, une police d’écriture, ou police de caractères, est un ensemble de glyphes, c’est-à-dire de représentations visuelles de caractères d’une même famille, qui regroupe tous les corps et graisses d’une même famille, dont le style est coordonné, afin de former un alphabet, ou la représentation de l’ensemble des caractères d’un langage, complet et cohérent.], (fonts) qui sont adaptés aux langues africaines.  Pour bien afficher le text, vous aurez besoin de polices qui gèrent les caractères unicodes suivants: Même des polices très répandues comme link:https://fonts.google.com/specimen/Roboto?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Roboto], (Google) ne gèrent pas les langues mandingues. Noto Sans ka ɲi ani Nunito ma ɲi dɛ! Polices d'écriture adaptées pour le bambara, le jula (et d'autres langues mandingues) Voici un link:https://www.coastsystems.net/dyu/form/ngalonci[court texte] qui permet de faire la vérification d'une police de caractères web et voir si elle peut afficher les caractères propre aux langues mandingues. [ "julakan", "langue", "web", "unicode"] ["Julakan", "Technologie" ] false fr  https://www.coastsystems.net/dyu/form/ngalonci polices.png post |Glyph IPA|Unicode
|ɔ	|"0254
|Ɔ	|"0186
|ɛ	|"025B
|Ɛ	|"0190	
|ɲ	|"0272
|Ɲ	|"019D 
|ŋ	|"014B
|Ŋ	|"014A
 Project-Id-Version: 
PO-Revision-Date: 2021-04-22 11:59+0000
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: fr
X-Generator: Poedit 2.4.2
 /images/polices.png 2020-10-21 This site uses as the default font link:https://fonts.google.com/specimen/Inter?preview.text=%C9%94%20%200254%20%C6%86%20%200186%20%C9%9B%20%20025B%20%C6%90%20%200190%20%20%20%C9%B2%20%200272%20%C6%9D%20%20019D%20%20%C5%8B%20%20014B%20%C5%8A%20%20014A&preview.text_type=custom&query=noto[Noto Sans] You can find other web fonts at...  link:https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Google Fonts] The browser will substitute missing glyphs usually from a system font. Often this substitution of glyphs from another system font may actually be acceptable or even very good.  However if you don't ever want any surprises in your web app or web page, just choose a font that is adapted to the language.  The most common Google web font that displays all the IPA glyphs is Noto Sans or Noto Serif.  We are also dealing specifically with manding languages here, but if your text is in Sénari(Senoufo) ou bien Wawle(Baoulé)  ou encore ˈBhɛtɩgbʋʋ (Bete), the font choice is all the more important. The internet is international, but not the case of all fonts.footnote [With the advent of digital typography, "font" is frequently synonymous with "typeface". Each style is in a separate "font file"—for instance, the typeface "Bulmer" may include the fonts "Bulmer roman", "Bulmer", "Bulmer bold" and "Bulmer extended"—but the term "font" might be applied either to one of these alone or to the whole typeface.] Even some very common fonts such as link:https://fonts.google.com/specimen/Roboto?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Roboto], (Google) do not display the manding glyphs without help from the browser that performs character substitution. Noto Sans ka ɲi ani Nunito ma ɲi dɛ! Appropriate fonts for Bambara, Jula (and other Manding languages) Here is a link:https://www.coastsystems.net/dyu/form/ngalonci[court texte] to a text showing several web fonts that correctly display the glyphs necessary for mandinq languages. [ "jula", "language", "web", "unicode"] ["Julakan", "Technology" ] false en  https://www.coastsystems.net/dyu/form/ngalonci polices.png post |Glyph IPA|Unicode
|ɔ	|"0254
|Ɔ	|"0186
|ɛ	|"025B
|Ɛ	|"0190	
|ɲ	|"0272
|Ɲ	|"019D 
|ŋ	|"014B
|Ŋ	|"014A
 