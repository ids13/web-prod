��    2      �  C   <      H     I     `     t      �  �  �     �  �   �  L   !     n     �  M   �  B   �  C   ,     p  S  �  v   �	  C   O
  6  �
     �  �   �     �  �   �     t  x   �  �   �  W   �     �     �  3      (   4     ]  I   a  ~   �  $   *     O  !   ]  +     $   �  )   �     �            
        &  
   2     =  �   B        ^   6  �   �     �     �     �     �  t  �     \  n   n  .   �            I   /  5   y  6   �     �    �  Y     6   l    �     �  �   �     f  }        �  K     �   h  L      
   M     X  3   i  (   �     �  ;   �  �     "   �     �  #   �  *   �  ,     )   4     ^     j     v  
        �  
   �     �  �   �     �     �                         +               "      '             /   
                    $                        ,   &                    )             *                             !   2              1      %      0   -   	   #   .      (    /images/nvim_94554.png 2020-05-14T13:21:14 Activer un module Alors ce n'est pas compliqué ! Alors si on abandonne son gestionnaires de module préféré, comment on fait ? Eh bien la plupart des gens utilisent déjà git.  Alors remplacer les quelques 4 ou 5 commandes de votre ancien gestionnaire de module avec une commande git correspondante.  Et le bienfait supplémentaire c'est que vous n'aurez *rien* de plus dans votre ficher init.  Vous avez déjà installé Vim ou Nvim ? Laissez le faire le travail ici ! Les commandes git dont vous aurez besoin sont : C'est tout ! A+ Certains vont affirmer que vimplug ou bien d'autres gestionnaires simplifient ces taches.  En tant que minimaliste, je ne suis pas d'accord. D'abord quels sont les opérations courantes d'un gestionnaire de modules ? Désactiver un module Désinstller un module Gérer les modules Vim et Nvim facilement avec les gestionnaires inégrés ! Gérer les modules vim sans soucis (abandonner le Gestionnaire !) Gérer les modules vim sans soucis (abandonner le Gestionnaire !)  Installer un module J'ai même installé minpack et puis rapidement ne n'en ai pas vu le besoin. Pourquoi donner une instruction à un élément externe pour charger un module que vim peut faire sans *aucune* configuration ! Et la configuration supplémentaire a fini par encombrer mon fichier init.vim avec les instructions de minpac que vim fait lui-même. L'avantage inattendu de cette façon est que vous n'avez plus de tracas de configuration de votre .vimrc ou init.nvim. La preuve ? Voici mon fichier de configuration init.nvim actuel : Les versions récentes de Vim et Nvim sont équipées de gestionnaire de modules, mais ce n'est pas tout le monde qui en profite.  Eh bien voici quelques raisons d'abandonner Pathogen, Vim-Plug, Vundle, et même minpac (qui est quand même basé sur le gestionnaire intégré pour gérer le chemin d'execution) Lister les modules installés Maintenant si vous êtes sur n'importe quelle autre machine, vous pouvez lancer « git clone myplugins » ;  Ensuite « git submodule init --recursive..  Et vous venez d'installer tous vos modules. Mettre à jour les modules Pour démarrer cette nouvelle façon de faire vous n'avez que vous rendre dans votre répertoie de modules et créer les sous répertoires s'ils n'existe pas déjà. Quoi d'autre ? Se nécessaire, vous devez créer un dépot git dans ce répertoire « plugins » et faire un push vers github/gitlab. Si vous désinstallez un module, vous aurez aussi besoin de lancer une deuxièment commande pour vous débarraser de toutes ses traces : Vous pouvez bien sûr utiliser des alias bash si ces commandes semblent trop longues ! ["Technologie"] ["linux", "vim"] a|image::vim_94609.png[]
a|image::nvim_94554.png[]
 filetype plugin indent on
syntax enable
 fr  git commit -a -m "Installé mon nouveau module" && git push origin master git init
git add *
git commit -a -m "mes modules"
git add remote [url du projet de votre compte git]
git push -u orign master
 git rm --cached [chemin du module] 
 git submodule git submodule add [url de github] git submodule deinit {start}/{opt}/[module] git submodule mv start/[module] /opt git submodule update --remote --recursive plugdisable pluginstall pluglist plugremove plugsaveall plugupdate post set encoding=utf-8
set hidden
set noerrorbells
set smartindent
set smartcase
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set path+=**
set wildmenu
set ts=2
set sw=2
set expandtab
set nowrap
 silent! helptags ALL
 vim:  cd ~/.vim/pack/plugins
nvim: cd /.local/share/nvim/site/pack/plugins/
mkdir {opt,start}
 Project-Id-Version: 
PO-Revision-Date: 2021-04-23 14:25+0000
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: fr
X-Generator: Poedit 2.4.2
 /images/nvim_94554.png 2020-05-14T13:21:14 Enable a plug-in So really its not complicated! So if you don't use a plugin manager how do you get along? Well most people use git, and you just replace the 4 or so commands used by your plug-in manager by a corresponding git command.  And the added benefit, is you need less or nothing extra in your init file.  You have already installed Vim or Nvim.  Let Vim or Nvim do the work here! The corresponding commands are: That's all folks! Some would argue that vimplug or others simplify these tasks.  But as a minimalist, I don't actually think so. OK, so what can you do with a plug-in manager? Disable a plug-in Remove a plug-in Simplify your Vim plug-ins with Vim and nvim integrated plug-in managers! Simplified Vim plugin management (ditch the manager!) Simplified Vim plugin management (ditch the manager!)  Install a plug-in I even installed minpack and then didn't really see the need for it.  Why instruct some external piece to load a package when vim does it without any corersion? And the extra configuration just ended up cluttering my init.nvim file with minpack install stuff that git already does. The added benefit of this is you now have zero plug-in stuff in your .vimrc or init.nvim. As proof, Here is the actual contents of my init.nvim: Recent version of Vim and Nvim include a plug-in manager, but it seems that not everyone has yet caught yet.  Well here are my reasons to abandon Pathogen, Vimplug, and even minpac (which is based on the new integrated plugin managers and uses the appropriate runtime path) List installed plug-ins Now if you are on any other machine you can just git clone myplugins.  Then do a git submodule init --recursive.  And you have all your plug ins set up. Update installed modules To set this up you just need to go to your plug-ins directory and create the directory structure if it doesn't already exist. Is there really anything else? If necessary, you should make this a git repo and push it to github/gitlab. And I do have to add a small caveat here.  If you do remove a submodule you will also have to run an additional command to get rid of all traces of it: Hey you can even use a bash alias if those submodule commands look too long! Technology ["linux", "vim"] a|image::vim_94609.png[]
a|image::nvim_94554.png[]
 filetype plugin indent on
syntax enable
 en  git commit -a -m "Install plugin" && git push origin master git init
git add *
git commit -a -m "my plugins"
git add remote [ project url from your git account ]
git push -u origin master
 git rm --cached [path to module] 
 git submodule git submodule add (url from github) git submodule deinit [start/(opt)/plug-in] git submodule mv [start/plugin] [opt/plugin] git submodule update --remote --recursive plugdisable pluginstall pluglist plugremove plugsaveall plugupdate post set encoding=utf-8
set hidden
set noerrorbells
set smartindent
set smartcase
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set path+=**
set wildmenu
set ts=2
set sw=2
set expandtab
set nowrap
 silent! helptags ALL
 mkdir {opt,start}
 