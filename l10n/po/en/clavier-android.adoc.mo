��            )   �      �     �     �     �  �   �  ;   d     �     �  <   �  U     �  X  A   "  m   d  |   �  9   O  +   �  #   �    �    �     �	     
  #   ,
     P
  V   T
  \   �
                    '  3   ,  �   `     W     s     �  v   �  4        A     S  .   f  K   �  z  �  6   \  U   �  x   �  /   b  %   �     �  *  �  %       '     C     _     ~  V   �  S   �     -     A     D     L  3   Q              	                                                                                                                   
        /images/clavier_android.png 2020-04-23T20:32:08Z A ka nɔgɔ! A partir de ces options vous pouvez aussi choisir une configuration azerty, qwerty, et dans le cas de Jula, il y même l'option Nko! Allez d'abord aux Paramètres, Système, Langues et saisi.
 Author Boyd Kelly Ayiwa.  An ka taa! Ensuite notez bien la partie *'Clavier et modes de saisie'*
 Ensuite taper sur *'Langues'*.
a|image::Screenshot_20200424-191500.png[keyboard,400]
 Maintenant le clavier GBoard de Google est disponible en plus de 500 langues! Et bien qu'on l'attend toujours en Senoufo, Guere, et Yacouba entre autres, déjà les claviers Baoulé et Jula sont disponibles.  Puisque ces claviers sont installés au niveau du système, ils sont disponibles dans toute les applications.  Alors que vous allez envoyer un mail, taper un message sur les réseaux sociaux ou bien écrire une lettre vous n'aurez pas de problème! Pouvez-vous écrire ça sur votre clavier de portable ?  Si !   Si maintenant vous faites une longue presse sur la barre d'espace, vous aurez l'option de changer de clavier. Si vous utilisez déjà Gboard, ou bien vous venez de l'installer, maintenant il s'agit d'activer la langue que vous voulez. Tapez en Baoulé ou Julakan comme un boss sous Android ! Tapez sur l'option *'Clavier virtuel'*.   
 Trouvez *Dioula* ou bien *Baoulé*
 Vous devez avoir le clavier GBoard installé sur votre appareil.  Par contre, Samsung par exemple utilise son propre clavier (moins performant) par défaut.  Il se peut que vous devez aller sur Google Play, chercher gboard, puis installez ce clavier, et l'activer par défaut. Vous pouvez taper sans problème le français sur le clavier Julakan ou Baoulé.  Par contre le correcteur d'orthographe est lié au clavier choisi.  Donc vous trouverez sans doute avantage de basculer entre les claviers.  Mais une touche longue et c'est fait. [ "Technologie", "Julakan" ] [ "Technologie", "langue" ] [ "julakan", "android", "afrique" ] a|
 a|image::Screenshot_20200424-191237.png[keyboard,400,,float="left",align="center"]
a|
 a|image::Screenshot_20200424-191614.png[keyboard,400]
a|* Tapez sur *'Ajouter un clavier'*

 clavier_android.png fr julakan post « I ni sɔgɔma! Koow be di?  Bara ka ɲi wa? » Project-Id-Version: 
PO-Revision-Date: 2021-04-26 19:17+0000
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: fr
X-Generator: Poedit 2.4.2
 /images/clavier_android.png 2020-04-23T20:32:08Z A ka nɔgɔ! From these options, you can choose a keyboard configuration, azerty, qwerty or with Jula, there is even an Nko option! First go to Settings, System, Languages and inputs.
 Author Boyd Kelly Ayiwa.  An ka taa! Notice the section *Keyboard and input modes*
 Then select *Language*
image::Screenshot_20200424-191500.png[keyboard,400]
 The Google Gboard keyboard is available in more than 500 languages! While we are still waiting for it thought in Senoufo, Guere and Yacouba , at this time its available in Baouké and Jula.  Since these keyboards are system utilities, they are available in all applications.  So if you want to send an email, post a message on a social network, or write a letter, you can do it! Can you type that on your phone keyboard?  Of course!  If you do a long press on the space bar you will find the option to switch keyboards. If you already use Gboard, or if you have just installed it, now its a matter of activating the language input you wish. Type like a boss in Jula or Baoulé in Android! Select the option *Virtual Keyboard*
 * Find *Dioula* or *Baoulé*
 You must have the GBoard keyboard installed on your device.  Note however that Samsung uses its own (less powerful)  keyboard by default.  You may have to go to the Google Play Store, search for 'gboard' and install this on your device.  Then you will need to activate this as the default keyboard. You can type in English with no issues withe the Julankan or Baoulé keyboards.   However the spell check is tied to the keyboard.   So you will undoubtedly find advantageous to switch keyboards according to the language in which you are typing.  One long press on the kbd::space and its done! [ "Technology", "Julakan" ] [ "Technology", "Julakan" ] ["language", "tech", "africa"] a|
 a|image::Screenshot_20200424-191237.png[keyboard,400,,float="left",align="center"]
a|
 a|image::Screenshot_20200424-191614.png[keyboard,400]
a|* Select *Add a keyboard*

 clavier_android.png en julakan post « I ni sɔgɔma! Koow be di?  Bara ka ɲi wa? » 