��    #      4  /   L        )   	  �   3          )     >  "   [  	   ~  �   �  %     @   7  
   x     �  �   �  �   U  �   I  E   
     P    i    |	  6   �
  �  �
     D     P  (   `  4   �     �  �   �     c          �     �  J   �  W   �  Y   ;  �   �  )   �  �   �     �     �     �     �     �  x   �     w  4   �  
   �     �  �   �  �   �  �   �  >   G     �  �   �  �   �  .   _    �     �     �  '   �  4   �       �        �     �     �     �  J   �  (   <  @   e                       !                                                 "              
   #                                                         	           "highlight lCursor ctermbg=red guibg=red
 "keymappings unique to french
set spelllang=fr
set keymap=french
:imap <expr> " getline('.')[col('.')-2]=~'\S' ?  ' »' : '« '
":imap <expr> ' getline('.')[col('.')-2]=~'\S' ?  ' »' : '« '
set background=dark
set wordwrap
 /images/vim.png 2020-04-13T00:00:00Z ?	  ?
!   !
:   :
;   ;
 Allez vous essayer ?  Formidable ! Allons ! Alors si vous changer votre dictionnaire pour corriger l'orthographe en français, les options de clavier vont suivre automatiquement ! Avec le contenu très simple suivant: Avec mon éditeur préféré Nvim sous Linux c'est chose facile. Boyd Kelly Ce fichier contient le suivant: Ce fichier va aussi change la palette de couleurs selon que la langue du fichier et détermine aussi une option de retour automatique (Davantage sur ce sujet une prochaine fois) Ce fichier va bien s'occuper des marques d'interrogation et d'exclamation, les deux-points et le point-virgule. Quant aux guillemets on va créer un fichier de configuration, spécifique « french.vim » qui se trouvera à l'endroit suivant: Ce fichier va gérer non seulement le chargement du « keymap », mais va aussi remplacer en temps réel les " anglais avec les « ou » selon la position et y compris l'espace insécable. Créer un ficher à l'endroit suivant pour soit vim/gvim ou bien nvim La légende des siècles Les espaces qui se trouvent devant les points d'exclamation et d'interrogation sont des espaces insécables.  On peut entrer ces espaces Unicode sous Neovim et vim en tapant kbd:[Ctrl + V + u + 0 + 0 + a + 0 ] et normalement vous pouvez aussi couper-coller le texte ci-haut. Maintenant il y a plusieurs façons d'activer ces fichiers.
 Une façon simple de choisir les options en français est d'utiliser la fonction spelllang pour appliquer le keymap.
 Vous pouvez accomplir cela avec ce qui suit dans votre ~/.vimrc ou ~/.config/nvim/init.vim:
 Ponctuation française facile sous Linux avec nvim/vim Si vous avez l'habitude de taper en anglais et en français, comme moi, comment gérez vous les espaces qui précèdent les points d'interrogation et d'exclamation, les deux-points, le point-virgule, et les guillemets ? En français, selon les normes (?? en tout cas), il faut avoir des espaces avant le point d'interrogation et le point mais les anglais sont étonnés de voir cela. Victor Hugo ["Technologie"] ["vim", "français", "langue", "linux" ] autocmd OptionSet spelllang call LanguageSettings()
 fr function! LanguageSettings()
  if &spelllang =~ 'fr'
		source ~/.config/nvim/fr.vim
	elseif &spelllang =~ 'en'
		source ~/.config/nvim/en.vim
  endif
endfuction
 let b:keymap_name="French"
 loadkeymap
 post vim.png |(vim/gim):
|~/.vim/keymap/french.vim
|(nvim):
|~/.config/nvim/french.vim
 |(vim/gvim): 
|*.vim/keymap/fr.vim)*
|(nvim): 
|*.local/share/nvim/site/keymap/fr.vim*
 « Les sauterelles envahissent l’Égypte et les virgules envahissent la ponctuation ». Project-Id-Version: 
PO-Revision-Date: 2021-04-23 14:26+0000
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: fr
X-Generator: Poedit 2.4.2
 "highlight lCursor ctermbg=red guibg=red
 "keymappings unique to french
set spelllang=fr
set keymap=french
:imap <expr> " getline('.')[col('.')-2]=~'\S' ?  '��' : '��'
":imap <expr> ' getline('.')[col('.')-2]=~'\S' ?  '��' : '��'
set background=dark
set wordwrap
 /images/vim.png 2020-04-13T00:00:00Z ?	  ?
!   !
:   :
;   ;
 Try it! Lets get to it! Now if you change the spelling dictionary to French, the keyboard configuration options will be activated automatically! With the following contents: With my preferred editor Nvim on Linux this is easy. Boyd Kelly With the following content: This file for me also makes a change to the colorscheme when I'm editing a file of a particular language, and also sets the wordrap option.  (More about that another time and space!) This takes care of the question mark, exclamation mark, colon and semi colon.  The quote marks need a bit more work.  For those lets create a specific "french.vim" configuration file which will also load the preceding keymap in: This preceding file now takes care of loading the keymap, and also replacing the English " with either ��or�� depending on the position and also including the non breaking space. Create a file at the following location for either Vim or Nvim La légende des siècles The spaces before the punctuation marks are nonbreakable spaces.  These unicode characters can be entered in nvim/vim by pressing kbd:[Ctrl + V] kbd:[u] kbd:[0] kbd:[0] kbd:[a] kbd:[0] and normally you should also be able to cut and paste the text above. Now there are several ways to load this. 
One simple way is to trigger this by the spelllang option.
You can accomplish this by putting the following in your .vimrc or .config/nvim/.init.vim:
 Easy French Punctuation on Linux with Nvim/Vim If you like me type a lot in both English and French, how do you deal with the spaces before question marks, exclamation marks colons, semi-colons, as well as quote marks.  In French these spaces are mandatory although misunderstood for some English speakers as a mistake! Victor Hugo ["Technology"] ["vim", "french", "language", "linux" ] autocmd OptionSet spelllang call LanguageSettings()
 en function! LanguageSettings()
  if &spelllang =~ 'fr'
		source ~/.config/nvim/fr.vim
	elseif &spelllang =~ 'en'
		source ~/.config/nvim/en.vim
  endif
endfuction
 let b:keymap_name="French"
 loadkeymap
 post vim.png |(vim/gim):
|~/.vim/keymap/french.vim
|(nvim):
|~/.config/nvim/french.vim
 |*.local/share/nvim/site/keymap/fr.vim*
 The grasshoppers invade Egypt and the commas invade punctuation. 