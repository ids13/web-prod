��          �      �       H     I  
   ^  ]   i  /   �    �          1     L     h     n     r  !   w    �  �   �     �  
   �  8   �     �              2     O     j     p     t  !   y    �                    	                       
                      /images/fontlist.png 2020-11-08 Liste de toutes les polices d'écriture web (chez Google) qui gèrent les langues mandingues. Polices d'écriture web pour langues mandingues Toutes les polices d'écriture web (chez Google) qui gèrent les langues mandingues.footnote:[On peut acheter d'autres polices; Par contre de nos jours connectés sur le web, ces polices seront probablement remplacées par une police web dans une application ou navigatuer du client.]  [ "jula", "langue", "web" ] [ "jula", "langue", "web"] ["Julakan", "Technologie" ] false fr  post |image:fontlist.png[role='left']
 |link:https://fonts.google.com/specimen/Noto+Sans?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Noto Sans]
|link:https://fonts.google.com/specimen/Istok+Web?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Istok Web]!footnote:attention[Attention aux fontes gras et italiques]
|link:https://fonts.google.com/specimen/Source+Sans+Pro?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Source Sans Pro].footnote:attention[]
|link:https://fonts.google.com/specimen/Noto+Serif?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Noto Serif]
|link:https://fonts.google.com/specimen/Fira+Sans?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Fira Sans]
|link:https://fonts.google.com/specimen/Fira+Sans+Condensed?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Fira Sans Condensed]
|link:https://fonts.google.com/specimen/Fira+Sans+Extra+Condensed?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Fira Sans Extra Condensed]
|link:https://fonts.google.com/specimen/Arimo?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Arimo]
|link:https://fonts.google.com/specimen/Inter?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Inter]
|link:https://fonts.google.com/specimen/Questrial?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Questrial]
|link:https://fonts.google.com/specimen/Cardo?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Cardo]
|link:https://fonts.google.com/specimen/M+PLUS+Rounded+1c?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[M Plus Rounded 1c]
|link:https://fonts.google.com/specimen/EB+Garamond?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[EB Garamond]
|link:https://fonts.google.com/specimen/Tinos?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Tinos]
|link:https://fonts.google.com/specimen/Balsamiq+Sans?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Balsamiq Sans].footnote:[Certains glypes sont disproportionés]
|link:https://fonts.google.com/specimen/M+PLUS+1p?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[M Plus 1p]
|link:https://fonts.google.com/specimen/Didact+Gothic?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Diadact Gothic]
|link:https://fonts.google.com/specimen/Cousine?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Cousine]
|link:https://fonts.google.com/specimen/David+Libre?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[David Libre]
|link:https://fonts.google.com/specimen/Gothic+A1?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Gothic A1]
|link:https://fonts.google.com/specimen/Gentium+Basic?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Gentium Basic]
|link:https://fonts.google.com/specimen/Gentium+Book+Basic?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Gentium Book Basic]
|link:https://fonts.google.com/specimen/Andika?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Andikaa]
|link:https://fonts.google.com/specimen/Voces?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Voces]
 Project-Id-Version: 
PO-Revision-Date: 2021-04-22 11:57+0000
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: fr
X-Generator: Poedit 2.4.2
 /images/fontlist.png 2020-11-08 Complete list of (Google) Web fonts for Mande languages. Web fonts for Mande languages List of all (Google) web fonts that are usable with Mande languages.footnote:[Other fonts are available for purchase; However in our connected world of web apps, these fonts will most likely be substituted by another font by a client application or web browser.]  [ "jula", "language", "web"] [ "jula", "language", "web"] ["Julakan", "Technology" ] false en  post |image:fontlist.png[role='left']
 |link:https://fonts.google.com/specimen/Noto+Sans?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Noto Sans]
|link:https://fonts.google.com/specimen/Istok+Web?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Istok Web]!footnote:attention[Attention aux fontes gras et italiques]
|link:https://fonts.google.com/specimen/Source+Sans+Pro?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Source Sans Pro].footnote:attention[]
|link:https://fonts.google.com/specimen/Noto+Serif?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Noto Serif]
|link:https://fonts.google.com/specimen/Fira+Sans?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Fira Sans]
|link:https://fonts.google.com/specimen/Fira+Sans+Condensed?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Fira Sans Condensed]
|link:https://fonts.google.com/specimen/Fira+Sans+Extra+Condensed?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Fira Sans Extra Condensed]
|link:https://fonts.google.com/specimen/Arimo?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Arimo]
|link:https://fonts.google.com/specimen/Inter?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Inter]
|link:https://fonts.google.com/specimen/Questrial?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Questrial]
|link:https://fonts.google.com/specimen/Cardo?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[Cardo]
|link:https://fonts.google.com/specimen/M+PLUS+Rounded+1c?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom&categories=Serif,Sans+Serif,Monospace[M Plus Rounded 1c]
|link:https://fonts.google.com/specimen/EB+Garamond?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[EB Garamond]
|link:https://fonts.google.com/specimen/Tinos?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Tinos]
|link:https://fonts.google.com/specimen/Balsamiq+Sans?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Balsamiq Sans].footnote:[Certains glypes sont disproportionés]
|link:https://fonts.google.com/specimen/M+PLUS+1p?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[M Plus 1p]
|link:https://fonts.google.com/specimen/Didact+Gothic?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Diadact Gothic]
|link:https://fonts.google.com/specimen/Cousine?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Cousine]
|link:https://fonts.google.com/specimen/David+Libre?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[David Libre]
|link:https://fonts.google.com/specimen/Gothic+A1?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Gothic A1]
|link:https://fonts.google.com/specimen/Gentium+Basic?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Gentium Basic]
|link:https://fonts.google.com/specimen/Gentium+Book+Basic?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Gentium Book Basic]
|link:https://fonts.google.com/specimen/Andika?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Andikaa]
|link:https://fonts.google.com/specimen/Voces?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Voces]
 