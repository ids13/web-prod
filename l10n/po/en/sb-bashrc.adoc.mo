��          |      �          /   !  
   Q  �   \  "   �          -  C   A     �  |   �            �     (     
   0  �   ;  (   �     �     �  7        C  |   I     �     �                            
                    	           "Silverblue Tip - lancez des commandes partout" 2020-11-09 Est-ce que vous voulez utiliser régulièrement des utilitaires sur votre système hôte qui se trouvent dans un toolbox ? Mettez ceci dans votre ~/.bashrc: Silverblue Tip - commandes partout ["Technologie"] ["linux", "fedora"] ajouter [ -c nom-de-toolbox ] pour désigner un toolbox particulier false for bin in bat nvim code ranger anki meld poedit;
do
type -P $bin >/dev/null 2>&1 || alias $bin="toolbox run $bin" <1>
done
 fr post Project-Id-Version: 
PO-Revision-Date: 2021-04-23 14:26+0000
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: fr
X-Generator: Poedit 2.4.2
 "Silverblue Tip - Run commands anywhere" 2020-11-09 Do you find yourself regularly running utilities on your host system as well as in your toolbox?   Just put this in your ~/.bashrc: "Silverblue Tip - Run commands anywhere" ["Technology"] ["linux", "fedora"] add [ -c toolbox-name ] to run in a non default toolbox false for bin in fzf nvim code ranger anki meld poedit;
do
type -P $bin >/dev/null 2>&1 || alias $bin="toolbox run $bin" <1>
done
 en post 