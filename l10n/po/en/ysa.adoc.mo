��    #      4  /   L           	     #  1   )  ,   [  +   �     �  ;   �       ;        O  $   T  '   y  �   �  {   =  2   �     �                +     A  �   T     �     �  '        8     I     M  	   _     i  "   �  �   �     7     ;  u  @  .  �	     �
     �
  1     ,   7  +   d     �  ;   �     �  ;   �     +  $   0  '   U  �   }  {     2   �     �     �     �            �   0     �     �  '   �     $     7     ;  	   M     W  "   m  �   �     "     &    +           !                            	               #              
                                                                       "                  2018-05-21 22:46:26 +0000 Again C'est encore plus difficile que je ne le pensais. Elle dort encore.  (Elle continue à dormir) Elle mange encore.  (Elle mange de nouveau) Elle n'est pas encore venue Elle va encore.  (Elle continue d'y aller une fois de plus) Encore Encore (souvent traduit par 'même', mais parfois 'encore') Even Il y en a encore davantage à venir. It's even more difficult than I thought Nous avons non seulement quatre mots en anglais, mais quatre significations différentes avec la même traduction en français.  Connaissez-vous la nuance? Oui, parfois on trouve en anglais les deux mots ensemble dans une phrase : '*yet again*...' Qu'est-ce que cela signifie ? Quelle est la différence entre ces trois termes ? She hasn't come yet She is eating again She is going yet again She is still sleeping She is yet to come Souvent le français est plus précis que l'anglais... Surtout en matière de verbes.  Par contre l'anglais peut aussi être plus précis que le français. Still There's even more to come Voici quelques phrases pour vous aider: Voici un exemple Yet Yet Still Again ? Yet again ["Anglais en français"] ["langue", "français", "anglais"] et encore.footnote:[Dans link:https://www.coastsystems.net/en/post/ysa[la version anglaise] de ce document j'ai traduit 'encore' par 'aussi'.] : fr  post |Yet|Une action qui ne s'est pas produit jusqu'au présent. (encore)
|Still|Une action qui continue à se produire, qui n'a jamais arrêté.
|Again|Une action discontinue qui reprend.
|Even|Nuancé avec 'still'|Une chose qui s'est arrêtée/épuisée, mais il y en aura...|(encore !)
|Yet again|Une action discontinue qui reprend au moins une fois mais probablement plus.
 Project-Id-Version: Blog 1.0
PO-Revision-Date: 2021-04-26 18:38+0000
Last-Translator: Automatically generated
Language-Team: none
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
 2018-05-21 22:46:26 +0000 Again C'est encore plus difficile que je ne le pensais. Elle dort encore.  (Elle continue à dormir) Elle mange encore.  (Elle mange de nouveau) Elle n'est pas encore venue Elle va encore.  (Elle continue d'y aller une fois de plus) Encore Encore (souvent traduit par 'même', mais parfois 'encore') Even Il y en a encore davantage à venir. It's even more difficult than I thought Nous avons non seulement quatre mots en anglais, mais quatre significations différentes avec la même traduction en français.  Connaissez-vous la nuance? Oui, parfois on trouve en anglais les deux mots ensemble dans une phrase : '*yet again*...' Qu'est-ce que cela signifie ? Quelle est la différence entre ces trois termes ? She hasn't come yet She is eating again She is going yet again She is still sleeping She is yet to come French has a reputation of being more precise than English.  And it often is especially when it comes to verbs.  However sometimes English can be more precise than French. Still There's even more to come Voici quelques phrases pour vous aider: Here is an exam;le Yet Yet Still Again ? Yet again ["English in French"] ["langue", "français", "anglais"] et encore.footnote:[Dans link:https://www.coastsystems.net/en/post/ysa[la version anglaise] de ce document j'ai traduit 'encore' par 'aussi'.] : en  post |Yet|An action that has not (yet) occurred.
|Still|An ongoing action that has not stopped.
|Again|An action which stops and restarts.
|Even|Similar to 'still'|Something has stopped or is no longer but it will be again
|Yet again|An action that reoccurs at least once but probably more.
 