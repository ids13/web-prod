��    &      L  5   |      P  �  Q     �  
   	  �  '	  U   �
  }   B  H   �  "   	     ,  \   I  Z   �  O        Q     e    r  K   �  �   �  Z   e  �  �  5   �     �  �   �  �   f  ~   J     �  �   �  f   �     �               5     J     N  M   S  T   �  S   �  �   J  �   �  �  �     �  
   �  G  �  V      l   Z   @   �   #   !     ,!  Y   I!  W   �!  L   �!     H"     ]"     j"  8   k#  �   �#  R   =$  u  �$  2   &     9&  �   @&  �   '  k   �'     G(  �   M(  \   �(     9)     H)     g)     �)     �)     �)  F   �)  T   �)  R   A*  �   �*                                           %   	      $                         "                   #              &   
                                             !                       ____
  | 1 3| 1 = Maj,  3 = AltGr + Maj    (AltGr est du côte droit)
  | 2 4| 2 = normal, 4 = AltGr
   ¯¯¯¯
   ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ _______
  |    | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 0  | °  | +  | <--   |
  | ²  | &  | é ~| " #| ' {| ( [| - || è `| _ \| ç ^| à @| ) ]| = }|       |
   ========================================================================
  | |<-  | A  | Z Ʒ| E Ɛ| R  | T  | Y Ƴ| U Ʋ| I Ɩ| O Ɔ| P  | ¨  | $  |   , |
  |  ->| | a  | z ʒ| e ɛ| r  | t  | y ƴ| u ʋ| i ɩ| o ɔ| p  | ^  ̌| £ ¤| <-' |
   ===================================================================¬    |
  |       | Q  | S  | D Ɗ | F  | G Ŋ| H  | J  | K  | L  | M  | %  | µ  |   |
  | MAJ   | q  | s  | d ɗ | f  | g ŋ| h  | j  | k  | l  | m  | ù `| *  ́|   |
   ========================================================================
  | ^   | >  | W  | X  | C  | V Ʋ| B Ɓ| N Ŋ| ?  | .  | /  | §  |     |     |
  | |   | <  | w  | x  | c  | v ʋ| b ɓ| n ŋ| ,  | ;  | : ¯| ! ~|     |     |
   ========================================================================
  |      |      |      |                       |       |      |     |      |
  | Ctrl | Super| Alt  | Espace   Nobreakspace | AltGr | Super|Menu | Ctrl |
   ¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯ ¯¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯ ¯¯¯¯¯
----  
 /images/clavier_ivoirien.png 2020-11-06 Alors que plusieurs claviers unilingues exigent l'utilisation des touches qui interrompt la fluidité de dactylographie, celui-ci pour la plupart des glyphes permet les mains de rester normalement sur le clavier.  En outre les majuscules et minuscules ont les même 'mouvements' donc facilitant le mémoire musculaire. C'est donc un clavier de préférence de traducteur et écrivain.  Il y inclut aussi un clavier standard "Français (Côte d'Ivoire). Appuyer et tenir kbd:[AltGr] et ensuite la touche appropriée de la table ci dessous. Appuyer la touche kbd:[!] (sur le clavier azerty), relâche, et ensuite appuyer la touche appropriée de la table ci dessous. Appuyer la touche kbd:[!] deux fois pour insérer la mark d'exclamation. Clavier Ivoirien sans gymnastiques Distributions Debian/Ubuntu: Dépôt: link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/[Fedora] Dépôt: link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/[Suse] Dépôt: link:https://launchpad.net/~bkelly/+archive/ubuntu/xkb-data-ci[Ubuntu] Fedora (et Mageia): Installation Je suis très heureux de publier le 'Clavier Ivoirien' sur un dépôt publique pour distributions Linux rpm et deb.  (d'autres comme Arch, Alpine, Gentoo pourraient suivre)  Ce clavier permet de taper _aisément_ (*sans faire de gymnastiques avec les doigts*) les langues ivoiriennes. La deuxième méthode (préférée) permet de taper avec plus de fluidité. La première méthode peut être difficile surtout avec les majuscules et quand la touche en question se trouve sur la droite du clavier. Le clavier français de Côte d'Ivoire est identique au clavier national français azerty. Le clavier multilingue de Côte d'Ivoire (civ) est basé sur le clavier français azerty, mais inclut des glyphes Unicode supplémentaires pour les langues de la Côte d'Ivoire.  Pour faire le clavier le plus facile à utiliser, on a consulté les claviers de Togo, Nigeria, Mali et en particulier le Cameroun.  Le clavier peut être utilisé pour taper en l'Attié, Abé, Bambara, Baoulé, Français, Gueré, Jula, Senoufo, Yacouba, et d'autres langues. Les glyphes Unicode sont accessibles de deux façons: Notes d'usage: On peut l'installer sur les systèmes 'rpm': Fedora, Suse, Mageia etc, et Ubuntu.  Ajouter simplement le dépôt approprié disponible selon le système. Pour les cas plus rares, par exemple l'accent grave ou aigu sur le ɛ́ ɛ̀ ou n'importe quelle autre lettre, taper le ɛ avec la touche kbd:[!] (azerty) ou kbd:[;] (qwerty) suivi par kbd:[AltGr] + kbd:[,], kbd:[.] ou kbd:[/]. Si vous avez des patchs ou contributions, voir le code source sur https://github.com/boydkelly/xkeyboard-config-ci[github.com] Suse: Une fois le dépôt ajouté et le paquet xkb-data ou xkeyboard-config mise-à-jour, vous allez trouver des claviers disponible sous les paramètres langues et claviers de votre système. Utilisez Yast pour ajouter link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/ ["Technologie"] ["langue", "afrique"] ["langue", "tech", "afrique"] clavier_ivoirien.png fr  post sudo add-apt-repository ppa:bkelly/xkb-data-ci
sudo apt-get upgrade xkb-data
 sudo dnf copr enable boydkelly/xkeyboard-config-ci
sudo dnf update xkeyboard-config
 zypper add https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/ 
 |Cherchez 'français' ou 'Côte d'Ivoire' et vous allez les retrouver.|Basculez entre les claviers disponibles.
a|image:input_lang.png[]
a|image:switch_lang.png[]
 Project-Id-Version: 
PO-Revision-Date: 2021-04-23 14:29+0000
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Source-Language: fr
X-Generator: Poedit 2.4.2
    ____
  | 1 3| 1 = Shift,  3 = AltGr + Maj    (AltGr is on the right)
  | 2 4| 2 = normal, 4 = AltGr
   ¯¯¯¯
   ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ _______
  |    | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 0  | °  | +  | <--   |
  | ²  | &  | é ~| " #| ' {| ( [| - || è `| _ \| ç ^| à @| ) ]| = }|       |
   ========================================================================
  | |<-  | A  | Z Ʒ| E Ɛ| R  | T  | Y Ƴ| U Ʋ| I Ɩ| O Ɔ| P  | ¨  | $  |   , |
  |  ->| | a  | z ʒ| e ɛ| r  | t  | y ƴ| u ʋ| i ɩ| o ɔ| p  | ^  ̌| £ ¤| <-' |
   ===================================================================¬    |
  |       | Q  | S  | D Ɗ | F  | G Ŋ| H  | J  | K  | L  | M  | %  | µ  |   |
  | SHIFT   | q  | s  | d ɗ | f  | g ŋ| h  | j  | k  | l  | m  | ù `| *  ́|   |
   ========================================================================
  | ^   | >  | W  | X  | C  | V Ʋ| B Ɓ| N Ŋ| ?  | .  | /  | §  |     |     |
  | |   | <  | w  | x  | c  | v ʋ| b ɓ| n ŋ| ,  | ;  | : ¯| ! ~|     |     |
   ========================================================================
  |      |      |      |                       |       |      |     |      |
  | Ctrl | Super| Alt  | Space   Nobreakspace | AltGr | Super|Menu | Ctrl |
   ¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯ ¯¯¯¯¯¯¯ ¯¯¯¯¯¯ ¯¯¯¯¯ ¯¯¯¯¯
----  
 /images/clavier_ivoirien.png 2020-11-06 Many multilingual keyboards require key combinations that interrupt the
'flow' of typing.   This keyboard for the most common characters allows the
hands to remain positioned normally on the keyboard.   In addition capital
and small case have the same 'movements' facilitating muscle memory.   This
is very helpful for translators and writers.  The package also includes a
standard french azerty keyboard  "Français (Côte d'Ivoire)", a multilingual
layout "Côte d'Ivoire Multilingue", and a Qwerty layout, based on the
(excellent) French Canadian keyboard "Côte d'Ivoire Querty". Press and hold the kbd:[AltGr] followed by the appropriate key in the following table. Press the kbd:[!] (on the azerty keyboard), release and then press the appropriate key from the table below. Press the kbd:[!] twice to actually insert the exclamation mark. Ivory Coast Keyboard  no gymnastics Debian/Ubuntu distributions: Repo: link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/[Fedora] Repo: link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/[Suse] Repo: link:https://launchpad.net/~bkelly/+archive/ubuntu/xkb-data-ci[Ubuntu] Fedora (and Mageia): Installation I am very pleased to present the 'Ivorian Keyboard' on public repos pour rpm and deb based linux distributions.  (Others like Arch, Alpine, Gentoo may follow).   This keyboard allows '_easy_' typing (*without doing finger gymnastics*) of Ivorian languages. The second method (preferable) allows more fluid typing. The first method (found on many other keyboards) may be difficult, especially when the key in question is a capital letter on the right of the keyboard. The Ivory Coast French layout is identical to the French national azerty keyboard. The Ivory Coast multilingual (civ) keyboard est based on the French azerty
keyboard, but includes supplemental Unicode characters for the languages of
Ivory Coast.   This keyboard has been based on those from Togo, Nigeria,
Mali and in particular Cameroon.  It can be used to type Attié, Abé,
Bambara, Baoulé, French, Gueré, Jula, Senoufo, Yacouba, and other languages. The Unicode characters are accessible in two ways: Notes: The keyboard can be installed on rpm systems from Fedora, Suse, Mageia etc, and deb based Ubuntu varieties.  Just add the appropriate copr or ppa repo and update either the keyboard package or your system. For some more rare cases for example a grave or aigu accent on ɛ́ ɛ̀ or any other letter, type the ɛ with the kbd:[!] (azerty) or ; (querty)  followed by kbd:[AltGr] + kbd:[,] or kbd:[.] or kbd:[/]. If you have patches or contributions, see the code on link:https://gitlab.com/boydkelly/xkeyboard-config-ci Suse: After the repository is added and the package updated, you will find the keyboards available under the language input settings of your system. Use Yast and add link:https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/ ["Technology"] ["language", "tech", "africa"] ["language", "tech", "afrique"] clavier_ivoirien.png en  post sudo add-apt-repository ppa:bkelly/xkb-data-ci
sudo  upgrade xdb-data
 sudo dnf copr enable boydkelly/xkeyboard-config-ci
sudo dnf update xkeyboard-config
 zypper add https://copr.fedorainfracloud.org/coprs/boydkelly/xkeyboard-config-ci/
 |Search for 'french' or 'Côte d'Ivoire' and you will find them.|Switch between available keyboards.
a|image:input_lang.png[]
a|image:switch_lang.png[]
 