build:
	po4a fr.cfg
	#po4a en.cfg
	git add -f l10n/pot/*/*.pot
	git add -f l10n/po/*/*.po
	./fixquotes.sh
	git commit --quiet -a -m "update on: $(date)"
	poedit&


date := $(shell date -Ih)
publish:
	echo $(date)
	git commit --quiet -a -m "published on: $(date)"
	git push origin main --quiet

